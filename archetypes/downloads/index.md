---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
type: release
version: ""
linux_appimage: ""
linux_binary_32: ""
linux_binary_64: ""
windows_binary: ""
windows_xp_binary: ""
mac_binary: ""
source_code: ""
---
