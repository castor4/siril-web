---
title: Boutique
author: Vincent Hourdin
type: page
date: 2022-03-31T15:28:49+00:00
menu: "main"
weight: 9
---

Si vous aimez Siril et voulez que tout le monde le sache, ou juste avoir un peu plus de Siril dans votre vie, nous avons une boutique à disposition, pour des vêtements et accessoires, portants tous le logo de Siril. Nous gagnons une petite commission sur les achats dans la boutique, cela nous aide aussi de cette façon.


<img class='mb-2' src="/images/shop_overview.png" alt="PayPal QR Code" />

La boutique est disponible dans plusieurs pays, il faut bien choisir celle qui correspond au lieu de livraison, pour le calcul des frais et taxes :

* [Boutique au Canada](https://siril-shop.myspreadshop.ca/)
* [Boutique en France](https://shop.spreadshirt.fr/siril-astro/)
* [Boutique aux États-Unis](https://siril-shop.myspreadshop.com/)
* [Boutique en Australie](https://siril-shop.myspreadshop.com.au/)

On peut vérifier sur quel site on est en bas de la page de la boutique.

Si vous voulez nous aider d'une autre façon, voyez la page [contributing to Siril](https://gitlab.com/free-astro/siril/blob/master/CONTRIBUTING.md) (en anglais).

Merci pour votre soutien !
