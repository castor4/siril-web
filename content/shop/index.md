---
title: Shop
author: Vincent Hourdin
type: page
date: 2022-03-31T15:28:49+00:00
menu: "main"
weight: 9
---

If you like Siril and want everybody to see it, or just enjoy it more in your everyday life, we have set-up an online shop for siril clothes and accessories, all featuring the Siril logo. We get a small commission from what you will buy in the store, this also helps us in that way.


<img class='mb-2' src="/images/shop_overview.png" alt="PayPal QR Code" />

The shop is available from several countries and it's important to choose the correct one to get the best suited shipping, taxes and overall cost:

* [Shop in USA](https://siril-shop.myspreadshop.com/)
* [Shop in Canada](https://siril-shop.myspreadshop.ca/)
* [Shop in Europe](https://siril-astro.myspreadshop.net/)
* [Shop in Australia](https://siril-shop.myspreadshop.com.au/)

You can see on which site you are at the bottom of the shop page.


If you want to help us in other ways, see the [contributing to Siril](https://gitlab.com/free-astro/siril/blob/master/CONTRIBUTING.md) page.

Thanks for your support!
