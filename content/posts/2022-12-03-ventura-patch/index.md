---
title: Bug Fix for MacOS Ventura users
author: Cyril Richard
show_author: true
date: 2022-12-03T00:00:00+00:00
featured_image: dmgpackage.png
categories:
  - News

---
 
MacOS Ventura is the thirteenth major version of macOS, released recently. Currently, Siril cannot be executed correctly on it because it is impossible to use the mouse on the application. This bug, which is very annoying, is caused by low-level changes in Ventura that affect GTK, the library that manages the graphical interface of Siril. And all applications using GTK3 are affected by this bug, namely GIMP v2.99, RawTherapee, Inkscape, Darktable and many others...

Currently we are able to provide a version of Siril with a partial fix on GTK. This allows the user to use the mouse again, but there may be other downsides. Of course, we will keep you informed about the progress of this issue.

In the meantime, I invite you to go to the [Download](../../../download) page and download the version of Siril corresponding to the architecture of your Apple computer. These versions are brought by our new macOS maintainer, René de Hesselle.
