---
title: Python to generate Siril script for cooled OSC
author: Argonothe
date: 2020-02-17T07:34:09+00:00
featured_image: Capture-d’écran-du-2020-02-17-08-30-33.png
categories:
  - News
  - Tips

---

![](Capture-d’écran-du-2020-02-17-08-30-33.png)

A big thank you to Cecile for sharing, maybe it will give you some ideas, so follow this link: https://discuss.pixls.us/t/python-to-generate-siril-script-for-cooled-osc/16461

Clear skies,

Laurent
