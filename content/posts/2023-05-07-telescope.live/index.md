---
title: Siril and Telescope.Live Launch New Tutorial Series
author: FreeAstro team
show_author: true
date: 2023-05-07T00:00:00+00:00
featured_image: telescopelive.jpg
categories:
  - News

---
We are excited to share that Telescope Live, the online platform dedicated to
astrophotography and remote imaging, has created a new video tutorial series
for Siril, available for free for anyone. The 6 videos, totaling 39 minutes,
are specially targeted at beginners. They cover many operations required to
transform a raw dataset into a beautifully processed image.

{{<figure src="telescopelive.jpg" link="https://telescope.live/tutorials/ultimate-astrophotography-toolkit-beginners">}}

Telescope Live operates very high quality telescopes under the best skies
of both hemispheres and provides a huge number of pictures in their archive,
yet at an affordable price. They spent time learning to use Siril, creating
scripts, and producing the video tutorials, making them available for free. The
fact that this tutorial series is free and available to anyone is because they
share the same mission with us: making astrophotography accessible to everyone.

By choosing Siril for this task, it also reassures us in our view of Siril as a
great tool for beginners, as well as more experienced astrophotographers, for
those who want results quickly or in an automated way.

We warmly thank them.

[Click here to view the tutorials](https://telescope.live/tutorials/ultimate-astrophotography-toolkit-beginners)

[Learn more about Telescope Live](https://telescope.live/start (one-week free trial))

As a reminder, you'll find our in-depth, text and images, tutorials on
our [website](../../../tutorials).
