---
title: Siril joined Pixls.us, the largest free/open photography community
author: Argonothe
date: 2020-01-24T13:07:58+00:00
featured_image: /wp-content/uploads/2020/01/Capture-d’écran-du-2020-01-24-14-06-19-1024x409.png
categories:
  - News
---

We are proud to announce that Siril has joined [PIXLS.US](https://pixls.us), the largest Free/Open Source Photography community.

Like other softwares such as The Gimp, Darktable or RawTherapee we now have a forum at this address: <https://discuss.pixls.us/c/software/siril>

Thank you for asking your questions on this new forum.

Clear Skies,

Laurent
