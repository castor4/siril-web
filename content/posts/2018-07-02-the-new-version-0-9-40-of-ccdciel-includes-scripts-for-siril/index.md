---
title: The new version 0.9.40 of CCDciel includes scripts for Siril.
author: Argonothe
date: 2018-07-02T17:56:02+00:00
featured_image: /wp-content/uploads/2018/07/Capture-du-2018-07-02-19-30-59-1024x592.png
categories:
  - News
---

It is possible to use scripts to pre-process the new images automatically for example at the end of the nightly sequence.

<span id="result_box" class="" lang="en"><span class="">You can now have your pre-processed files ready and immediately start the interesting part of the process. 😉<br /> </span></span>

There is standard script you can use directly or if you have very specific needs you can modify them to make your own.  
See here: <a href="https://www.ap-i.net/ccdciel/en/documentation/image_preprocessing" target="_blank" rel="noopener">https://www.ap-i.net/ccdciel/en/documentation/image_preprocessing</a>
