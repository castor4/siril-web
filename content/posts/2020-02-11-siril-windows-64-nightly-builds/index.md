---
title: Siril Windows 64 nightly builds
author: Argonothe
date: 2020-02-11T07:17:22+00:00
draft: true
private: true
url: /2020/02/11/siril-windows-64-nightly-builds/
featured_image: /wp-content/uploads/2020/02/Capture-d’écran-du-2020-02-11-08-05-00.png
categories:
  - News
  - Tips

---
The development version of Siril is now available for Windows 64 users.

Many thanks to gaaned92.

You can find it here: <a href="https://keybase.pub/gaaned92/SirilNightlyBuilds/" target="_blank" rel="noopener noreferrer">https://keybase.pub/gaaned92/SirilNightlyBuilds/</a>

If you want to help us and you find a bug describe it here: <a href="https://gitlab.com/free-astro/siril/issues" target="_blank" rel="noopener noreferrer">https://gitlab.com/free-astro/siril/issues</a>

Clear skies,

Laurent

Source: <a href="https://discuss.pixls.us/t/siril-windows-64-nightly-builds/16360" target="_blank" rel="noopener noreferrer">https://discuss.pixls.us/t/siril-windows-64-nightly-builds/16360</a>
