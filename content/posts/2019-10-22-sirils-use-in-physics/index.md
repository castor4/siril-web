---
title: Siril’s use in physics
author: Argonothe
date: 2019-10-22T17:44:59+00:00
featured_image: Capture-d’écran-du-2019-10-22-19-43-36.png
categories:
  - News

---
Siril is not only used by amateur astrophotographers, but also by physicists from a completely different field!

<img loading="lazy" src="siril1.png" alt=""/> 

![Siril 1 image](siril1.png)

![Siril 2 image](siril2.png)


Source: <a href="https://doi.org/10.1016/j.radmeas.2019.106201" target="_blank" rel="noopener noreferrer">https://doi.org/10.1016/j.radmeas.2019.106201</a>
