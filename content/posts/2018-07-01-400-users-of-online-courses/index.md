---
title: 400 users of online courses
author: Argonothe
date: 2018-07-01T06:43:45+00:00
featured_image: /wp-content/uploads/2018/07/Capture-du-2018-07-01-08-41-28-1024x302.png
categories:
  - News
---

This morning you are 400 to use this <strong>free</strong> service: <a href="https://siril.linux-astro.fr/" target="_blank" rel="noopener">https://siril.linux-astro.fr/</a>

Nearly 3 new users every day and this in a constant way since the opening of the service in January.

A big thank you to all
