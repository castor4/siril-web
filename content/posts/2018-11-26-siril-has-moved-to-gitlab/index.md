---
title: SiriL has moved to Gitlab
author: Cyril Richard
date: 2018-11-26T21:20:52+00:00
featured_image: ittimes_news_detail_large.jpg.png
categories:
  - News
---

SiriL has moved to a new collaborative programming infrastructure hosted by [Gitlab][1]. The new URL is:

  * <https://gitlab.com/free-astro/siril>

On the end-user side, we also replaced our old bug submission form by the one from Gitlab.

 [1]: https://www.gitlab.com "GitLab website"
