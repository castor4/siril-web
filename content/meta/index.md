---
title: Meta
author: "Pat David"
type: "meta"
date: 2021-02-05T11:24:42-06:00
TOC: true
---

Welcome to your new website!
This is a short guide to help you get acquainted with what we made and how to use it effectively.


## Content Organization

Don't let all of the directories overwhelm you in the repository.
The primary places where you'll be posting content are most likely going to be under `content/posts/` and `content/download/`.


### Posts

**To create a new post** just create a new directory under `content/posts/` and add an `index.md` file there with the minimum frontmatter needed (`title`, `author`, and `date`).

For example:
`content/posts/2021-02-05-a-test-post/index.md`

With at least the frontmatter:
```yaml
---
title: "A Test Post"
author: "Pat David"
date: 2021-02-05T12:53:20-06:00
---
```

Keep any images associated with the post in the same directory right next to the `index.md` file (this makes it convenient and portable as well).

To create a translation simply include another file named `index.LANG.md`.
As an example, a french translation would be `index.fr.md`.

(Further information on how Hugo handles translation can be [found in the Hugo docs](https://gohugo.io/content-management/multilingual/#translate-your-content)).

In the end you should have something like this for your content:

```
content/posts/
├── ...
├── 2021-02-05-a-test-post
│   ├── another-image.png
│   ├── image1.png
│   ├── image2.jpg
│   ├── index.md
│   └── index.fr.md
├── ...
```

---

Posts represent normal "news" type items.
They are also "blog" type posts as well.
We don't really make a distinction technically as they're all considered information you want to communicate to visitors.


### Downloads

Similar to [posts](#posts) the primary difference will be the inclusion of extra frontmatter defining links to actual binary downloads.

This content type is located in the `content/download/` directory.
You can follow the same conventions as posts and create directories here with all of the associated content inside (such as the `index.md` and `index.fr.md` files).

The big different is the frontmatter.
You can define the release version and URLs to binaries for various platforms in the frontmatter to automatically have download buttons and links created. Here is an example of the frontmatter:
```yaml {hl_lines=["5-12"]}
---
title: "Siril 0.99.6 (1.0.0 beta2 testing)"
author: "Cyril Richard"
date: 2020-09-22T20:50:00+00:00
version: "0.99.6"
linux_appimage: "https://free-astro.org/download/Siril-0.99.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_99_6_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.6-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.6.tar.bz2"
---
```

The rest of the file is usually going to be the release news/notes.
The site will include any posts in this directory as if they're news items.


## Writing

Posts are generally written using [Markdown][].
(Technically they can be written in other formats but outside of HTML will require extra libraries.)

Refer to your favorite Markdown reference for how to use it.
This section is more of a general style guide and some reminders for hugo-specific stuff.

[Markdown]: https://gohugo.io/content-management/formats/#learn-markdown "gohugo: Learn Markdown"


### Heading

Generally the title of the page is given the `<h1>` heading in the HTML document - so I like to use `<h2>` as my highest-level heading:

```
## A Heading 2

or

A Heading 2
-----------
```


### Shortcodes

Use shortcodes to generate output for some common tasks like [YouTube video embeds][] and [adding images in figures][].
This will help output better semantic HTML and make things simpler for you.

[YouTube video embeds]: #youtube-shortcode
[adding images in figures]: #figure-shortcode


#### Figure Shortcode

When including images in your posts it's a little more semantically correct to include them as part of a `<figure>` HTML element.
This is easily done using [Hugo's figure shortcode][]:

[Hugo's figure shortcode]: https://gohugo.io/content-management/shortcodes/#figure

```markdown
{{</* figure src="image.png" alt="Alt Text" */>}}
```

This format allows you to include various other parameters (see the hugo docs for a full description):

* src
* link
* target
* rel
* alt
* title
* caption
* height
* width
* attr
* attrlink

Here's a more complete example and how it will render below it:
```markdown
{{</* figure src="/images/lede-hero/8f0txgJCYhid_1824x0_cczXV3LN.jpg" 
link="https://siril.org" 
title="Fancy Stars" 
caption="A caption for the image"  
attr="lock042" 
attrlink="https://siril.org" */>}}
```
Will render the following HTML:
```html
<figure><a href="https://siril.org">
    <img src="/images/lede-hero/8f0txgJCYhid_1824x0_cczXV3LN.jpg" alt="A caption for the image"> </a>
    <figcaption>
        <h4>Fancy Stars</h4>
        <p>A caption for the image
            <a href="https://siril.org">lock042</a>
        </p>
    </figcaption>
</figure>
```

{{< figure src="/images/lede-hero/8f0txgJCYhid_1824x0_cczXV3LN.jpg" 
link="#figure-shortcode" 
title="Fancy Stars" 
caption="A caption for the image"  
attr="lock042" 
attrlink="https://siril.org" >}}



#### YouTube Shortcode

It's easy to add YouTube videos [using a shortcode][] as well.
You only need the `videoID` to embed (`StTqXEQ2l-Y` in the below url):

```url
https://www.youtube.com/watch?v=StTqXEQ2l-Y
```

*You can also use `vimeo` in place of `youtube`.*

The shortcode is:

```markdown
{{</* youtube StTqXEQ2l-Y */>}}
```

The HTML it renders is:

```html
<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
  <iframe src="https://www.youtube.com/embed/StTqXEQ2l-Y" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen="" title="YouTube Video"></iframe>
</div>
```

{{< youtube StTqXEQ2l-Y >}}

[using a shortcode]: https://gohugo.io/content-management/shortcodes/#youtube



## Translations

There are a few places that translations are handled.
For content you can simply place a translated file next to the same name content file.
For instance, to translate post or download content just include `index.fr.md` next to the primary `index.md` file.

For specific strings used throughout the site it's best to check in the `i18n/` folder.
There is a language-specific `yaml` file there where you can translate strings that are used elsewhere in the site.
(Currently there's `en.yaml` and `fr.yaml`)
