---
title: Script automatisé de réduction des étoiles
author: Rich Stevenson
show_author: true
featured_image: Capture-01.png
rate: "1/5"
type: page
---

Ce tutoriel vous montre comment installer et utiliser un script qui automatise le processus de réduction des étoiles dans Siril. Avec ce script, vous pouvez rationaliser le processus de réduction d'étoiles dans vos images de ciel profond et obtenir des résultats étonnants en quelques secondes !

*Attention* : Le script nécessite `Siril1.2.0-beta1` ou plus récent, et `StarNet++ CLI` installé et configuré dans Siril.

Ce tutoriel est également disponible sur ma chaîne YouTube Deep Space Astro :

{{< youtube Na6GzKozpCI >}}

# Télécharger le script de réduction d'étoiles
Le script peut être téléchargé en utilisant ce [lien]([DSA]%20Star%20Reduction-MTF.ssf) (clic droit et enregistrer sous...).


Suivez les instructions de la [documentation](https://siril.readthedocs.io/fr/stable/Scripts.html#adding-custom-scripts-folders) pour sauver le script dans votre répertoire utilisateur et rafraichir la liste des scripts.

Vous êtes maintenant prêt à exécuter le script.

# Ouvrir l'image pour la réduction des étoiles.

Ouvrez dans Siril l'image dans laquelle vous voulez réduire les étoiles. L'image devrait déjà être étirée et traitée.

Pour de meilleurs résultats, l'image doit être au format `.fit`. Cependant, comme l'implémentation de StarNet dans Siril convertit les images, j'ai réussi à exécuter le script de réduction d'étoiles avec d'autres formats tels que tif et jpg.

# Exécutez le script de réduction d'étoiles.

Cliquez sur le menu Scripts, puis sur `[DSA] Star Reduction-MTF`.

  {{< figure src="Capture-03.png" caption="Exécuter le script de réduction des étoiles." >}}

Le processus peut prendre quelques minutes et vous pouvez observer la progression dans l'écran de la console. Une fois le script terminé, vous verrez votre image avec les étoiles réduites.

# Augmenter la réduction des étoiles

Vous pouvez réduire davantage les étoiles de l'une des deux manières suivantes.

1. Exécutez simplement le script une deuxième fois.
2. Modifiez les valeurs du script comme indiqué dans l'étape suivante.

# Modifiez les valeurs du script avant de l'exécuter.

1. Ouvrez le fichier `[DSA] Star Reduction-MTF.ssf` dans un éditeur de texte.
2. Diminuez les deux valeurs en surbrillance indiquées ci-dessous. Par exemple, passez de 0.20 à 0.10. Elles doivent toutes deux être réglées sur la même valeur.

{{< figure src="Capture-04.png" caption="Run the Star Reduction script." >}}

3. Enregistrez le script 
