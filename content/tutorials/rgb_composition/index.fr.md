---
title: Composition RVB
author: Vincent Hourdin
show_author: true
featured_image: RGB_composition2.fr.png
rate: "3/5"
type: page
---

{{< table_of_contents >}}

Qu'elles viennent d'une caméra monochrome ou d'une caméra couleur avec des
filtres à double bande fine, créer une image couleur à partir d'un jeu d'images
empilées monochromes est toujours un défi. De nombreuses combinaisons sont
possibles et l y a aussi des spécificités dans le traitement de chacune. Dans ce
tutoriel, nous allons couvrir les bases en découvrant les outils mis à
disposition dans Siril pour arriver à créer les compositions que vous aimez.

Ce tutoriel étend le précédent en montrant les possibilités de Siril 1.2. Au
lieu de reposer sur l'outil graphique de Composition RVB, nous avons maintenant
deux alternatives: Pixel Math et les scripts.

* L'outil de [Composition RVB](https://siril.readthedocs.io/fr/latest/processing/rgbcomp.html)
  est un outil graphique qui peut prendre jusqu'à 8 images d'entrée pour
  réaliser des compositions de couleur complexes. À chaque image est assignée une
  couleur qu'elle prendra dans le résultat, ce qui rend l'opération à la fois
  simple en terme de créativité et très difficile à gérer au niveau de la balance
  des couleurs. Aussi, il ne permet pas de réaligner les images avec de la
  rotation ou d'égaliser le niveau des images depuis son interface, donc ces
  opérations doivent être faites auparavant.
* [Pixel Math](https://siril.readthedocs.io/fr/latest/processing/pixelmath.html)
  est aussi un outil graphique pouvant prendre autant d'images que nécessaire
  pour les combiner avec des formules contenant des opérateurs mathématiques,
  statistiques ou à deux images. Il permet de contrôler de façon simple comment
  les couleurs sont mélangées, fournit aussi une prévisualisation, mais ne permet
  pas non plus de réaliser la registration.
* Depuis la version 1.2, toutes les fonctions de traitement d'image sont
  disponibles sous forme de [commandes](https://siril.readthedocs.io/fr/latest/Commands.html),
  ce qui signifie qu'elles peuvent être utilisées dans des
  [scripts](https://siril.readthedocs.io/fr/latest/Scripts.html), y compris une
  version simplifiée de Pixel Math et des opérations de combinaison RVB et L-RVB.
  Cette méthode ne dispose pas d'une interface graphique ou d'une
  prévisualisation, donc elle doit être utilisée soit pour des compositions très
  simples ou pour répliquer des compositions effectuées précédemment avec les
  deux autres outils.

# Préparer les images

Avant de combiner des images monochromes en une image couleur, il est souvent
utile de réaliser deux opérations préliminaires: leur alignement et
l'égalisation de leur niveaux.

## 1. Alignement des images

L'outil de Composition RVB est capable de réaligner les images chargées, mais
seulement avec les [méthodes de registration](https://siril.readthedocs.io/fr/latest/preprocessing/registration.html#algorithms)
simples qui ne font que de la translation: la registration à une étoile et la
registration basée sur les motifs. Ne pouvoir utiliser que la translation à
cette étape est souvent une trop grande limitation, c'est rare de pouvoir bien
aligner les étoiles dans les 4 coins de cette façon. De plus, l'outil ne permet
pas d'enlever les bordures d'images qui ne sont pas communes à toutes d'être
supprimées, ni d'utiliser des images provenant de capteurs différents, par
exemple une caméra monochrome pour la luminance et une caméra couleur pour les
couleurs. Il est toutefois capable de mettre à l'échelle des images réalisées
avec la même système mais avec un binning différent.

Pour dépasser ces limitations, et c'est de toute façon nécessaire pour les deux
autres outils, nous recommandons d'utiliser la nouvelle [registration globale à
deux passess](https://siril.readthedocs.io/fr/latest/preprocessing/registration.html#pass-registration)
avec le [cadrage sur zone commune](https://siril.readthedocs.io/fr/latest/preprocessing/registration.html#apply-existing-registration).
Mais cela fonctionne normalement sur une [séquence](https://siril.readthedocs.io/fr/latest/Sequences.html) d'images,
pas sur des images avec des noms différents, donc une intervention manuelle est
nécessaire: créer un nouveau répertoire, renommer ou convertir les images, faire
la registration et renommer les résultat...

Heureusement, avec Siril 1.2 nous avons publié un script appelé
`RGB_composition.ssf` qui aligne et combine trois images monochromes ensemble.
Pour l'utiliser, dans un répertoire vide, placez les images empilées en les
nommant R, G et B suivi de l'extension FITS configurée (.fit par défaut). Lancez
le script, il va aligner et recadrer les images à leur zone commune, et créer
une image couleur appelée `rgb.fit` à partir des trois images. Mettre plus
d'images dans le répertoire les alignera quand même toutes, on pourra juste
ignorer l'image couleur créée. Les images alignées seront nommées
`r_colors_00001.fit` et les numéros suivants, *dans le même ordre alphabétique
qur les images d'entrée*.

*Attention:* si toutes les images n'ont pas le même échantillonnage, ou si elles
ont été prises avec des réglages de binning différent ou avec des caméras
différentes, le mode de cadrage commun risque de ne pas donner comme résultat
des images de la taille attendue, celle de la plus grande des images. Cela
dépendra de l'image de référence choisie pour la séquence. Une intervention
manuelle pourrait bien être nécessaire pour [choisir la
bonne](https://siril.readthedocs.io/fr/latest/preprocessing/registration.html#reference-image).

**Cas d'images mono ET couleur:** bien qu'il soit possible de combiner une image
de luminance monochrome et une image couleur pour la colorer avec les nouveaux
outils comme nous le verrons plus loin, il n'est toujours pas possible de les
aligner dans cet état. Il faudra donc quand même passer par une extraction de
canaux, soit depuis l'[interface
graphique](https://siril.readthedocs.io/fr/latest/processing/extraction.html#split-channels),
soit en utilisant la [commande split](https://siril.readthedocs.io/fr/latest/Commands.html#split).

## 2. Égaliser les niveaux des images

Un problème commun avec la composition couleur est que certaines images ont
beaucoup plus de signal que les autres, et parfois des niveaux de fond de ciel
très différents aussi. Les utiliser directement rendra la balance des blancs et
couleurs presque impossible à gérer dans certains cas, quoi que corriger
seulement le fond de ciel après combinaison est chose aisée sur l'image couleur
(avec l'[outil de calibration des
couleurs](https://siril.readthedocs.io/fr/latest/processing/colors.html#manual-color-calibration)).

A cette étape nous nous attendons à avoir des images dont le gradient de fond de
ciel a déjà été
[supprimé](https://siril.readthedocs.io/fr/latest/processing/background.html),
soit pendant le pré-traitement sur les images calibrées individuelles, soit
juste avant d'arriver ici sur les images empilées.

Il y a plusieurs façons d'égaliser les niveaux d'images monochromes dans Siril,
chacune avec ses avantages et ses inconvénients, certaines s'appliquant plus à
des types d'images (de filtres) spécifiques. L'expérience vous dira quelle est
celle qui conviendra mieux à vos besoins... Créer une image couleur est un
processus très subjectif et personnel.

### 2.1 Utiliser le Linear Match

Cette technique ne fonctionne qu'avec des images déjà alignées. Elle consiste à
trouver un facteur qui minimisera les différences de valeurs entre les pixels
d'une image à égaliser et d'une image de référence. Choisissez une image de
référence qui a des niveaux de fond, de nébuleuses et d'étoiles qui vous
paraissent corrects.

Dans Siril, cet outil est disponible à la fois dans l'interface graphique (voir
[sa documentation
ici](https://siril.readthedocs.io/fr/latest/processing/lmatch.html)) et par la
comande `linear_match`. Nous allons expliquer comment utiliser l'outil
graphique, en l'illustrant avec ces images d'entrée:
{{<figure src="input_images.png" caption="Les images d'entrée" link="input_images.png" >}}

Pour commencer, ouvrez une des images à égaliser par rapport à la référence dans
Siril. Ouvrez l'outil `Linear Match`, qui se trouve dans le menu `Traitement
d'image` en bas de la liste. Choisissez l'image de référence que vous avez
choisie en y naviguant après avoir cliqué sur le bouton en haut de la fenêtre.
Enfin, cliquez sur `Appliquer`, vous devriez voir un changement dans l'image
chargée.

Fermez ensuite la fenêtre et **enregistrez** l'image. Notez que l'utilisation du
bouton ou du raccourci `Enregistrer` écrasera l'image d'entrée, il y a aussi un
bouton `Enregistrer sous` à droite du bouton `Enregistrer` si vous souhaitez
changer le nom de l'image modifiée.

{{<figure src="linear_match.fr.png" caption="La fenêtre de l'outil linear match, avec une image O-III chargée comme référence" >}}

Faites de même pour toutes les images sauf celle choisie comme référence et
n'oubliez pas de toutes les sauvegarder après la manipulation et avant d'en
ouvrir une nouvelle. Voici le résultat :
{{<figure src="lm_images.png" caption="Les images après application de l'outil Linear Match" link="lm_images.png" >}}

Nous pouvons voir que le fond de ciel est encore plus différent, mais
l'important est que les nébuleuses ont une luminosité similaire dans les trois
images. Le fond de ciel peut être corrigé après la composition des couleurs.

### 2.2 Utiliser Pixel Math
La normalisation d'images utilisée lors de l'empilement serait en fait très
bonne pour cette tâche. Elle peut en fait être utilisée dans l'[export de
séquence](https://siril.readthedocs.io/fr/latest/Sequences.html#sequence-export),
mais le même problème qu'avec la registration manuelle apparait:
la manipulation et le suivi des fichiers. Pour faire plus simple, cette
opération peut être reproduite en utilisant
[Pixel Math](https://siril.readthedocs.io/fr/latest/processing/pixelmath.html).
Comme pour la méthode de Linear Match, il faut bien choisir une image de
référence avec les niveaux que l'on veut donner aux autres.

Pixel Math peut mélanger ou modifier plusieurs images monochrome ou couleur en
utilisant des formules mathématiques, les statistiques des images ou même des
opérateurs sur les images. Une utilisation typique de cet outil serait de
synthétiser un nouveau canal couleur ou luminance à partir de plusieurs, par
exemple un rouge à partir d'une image prise au filtre rouge et une au filtre
H-Alpha, comme montré dans son [tutoriel](https://siril.org/fr/tutorials/pixelmath/).
Mais en fournissant les bonnes formules, on peut aussi ajuster les niveaux des
images.

Mettez cette formule pour normaliser une image par rapport à une référence, la
référence étant nommée `ref` et l'image à normaliser `image` ici, l'image
normalisée sera nommée `image2` (ajuster à vos besoin et faites le _pour toutes_
les images):

```
pm "$image$*mad($ref$)/mad($image$)-mad($ref$)/mad($image$)*median($image$)+median($ref$)"
save image2
```

Il est aussi possible de rentrer cette formule directement pendant la
composition des couleurs comme on le voit sur l'image ci-dessous, mais cela
devient moins facile de contrôler la balance des couleurs et d'autres effets de
mélange.

{{< figure src="pixel_math_normalization.png" caption="Normalization d'images directement depuis Pixel Math" link="pixel_math_normalization.png" >}}

### 2.3 Utiliser l'auto-stretch délié
Siril 1.2 a ajouté la possibilité de délier les canaux pour l'auto-stretch, et a
ajouté la [commande autostretch](https://siril.readthedocs.io/fr/latest/Commands.html#autostretch).
L'auto-stretch, ou étirement automatique, étire l'histogramme pour faire un pic
d'une taille donnée et le déplace à la position donnée (les paramètres sont
habituellement automatiques). Si les mêmes paramètres sont utilisés pour les
mêmes canaux, cela peut assez bien aligner leurs histogrammes, ce qui revient à
faire une égalisation de niveaux. Cette pratique a toutefois une **limite
importante**, elle ne peut fonctionner qu'avec une image couleur, donc avec
seulement 3 filtres et après avoir fait une composition RGB, et elle ne garde
pas les images linéaires, ou fournit un étirement préliminaire d'histogramme.

La quantité d'étirement peut être largement diminuée, seulement en utilisant la
commande, pour ne pas avoir d'effet contre-productif sur la suite du traitement
d'image. Nous recommandons en effet d'utiliser le nouvel outil d'étirement
hyperbolique généralisé
([GHS](https://siril.readthedocs.io/fr/latest/processing/stretching.html#generalised-hyperbolic-stretch-transformations-ghs))
au lieu de l'outil classique de [Transformation
d'Histogramme](https://siril.readthedocs.io/fr/latest/processing/stretching.html#midtone-transfer-function-transformation-mtf)
parce qu'il fournit plus de contrôle, mais il fonctionne justement mieux avec un
pré-étirement.

### 2.4 Utiliser la Correction de Couleur Photométrique
Aussi limitée à un seul type d'image (3 canaux couleurs pris avec les filtres
rouge, vert et bleu, encore avec des valeurs linéaires), cette méthode est très
performante s'il y a assez d'étoiles non-saturées et pas de gradient évident
dans les images. La
[PCC](https://siril.readthedocs.io/fr/latest/processing/colors.html#photometric-color-calibration)
applique un facteur à chaque canal pour faire correspondre les couleurs des
étoiles à leurs couleurs réelles, et réaligne les fonds, éventuellement dans une
zone sélectionnée, pour avoir un gris neutre.

Comme il ne fonctionne qu'avec des images couleur, ce type d'égalisation arrive
après recombinaison des couleurs, et comme il analyse la couleur des étoiles,
les différents canaux couleurs doivent être bien alignés.

# Créer une image couleur

Une fois que toutes les images ont été alignées et cadrées, et en option que
leurs niveaux ont été égalisés, il est temps de créer une image couleur. C'est
généralement assez simple quand il n'y a que 3 images d'entrée, comme pour les
compositions RVB ou SHO, mais peut devenir assez complexe quand une image est
utilisée comme *Luminance* et que d'autres sont utilisées pour la colorer, ou
quand il n'y a pas 3 images.

La règle de base pour la composition basée sur une luminance est que les images
d'entrée doivent avoir préalablement eu leur histogramme étiré. Les images
linéaires sont très sombres et ne donneraient que peu de couleurs à l'image
résultante sinon.

Une autre préoccupation importante est de réfléchir à comment les images
d'entrées monochrome vont colorer l'image résultante. Avec 3 images d'entrée,
c'est généralement aussi simple que d'assigner une à chaque canal de destination
rouge, vert et bleu. Mais avec 2 ou plus de 3 images, une duplication ou un
mélange vont être nécessaire. Par exemple, avec 4 images faites avec des filtres
R, V, B, Ha, comment obtenir une image couleur ? De nombreuses réponses sont
possibles... Favoriserez-vous des couleurs réalistes ? Dans ce cas, le H-Alpha
est supposé être rouge. Ou voudriez vous faire ressortir plus de détails, que le
H-Alpha pourrait fournir ? Dans ce cas, peut-être qu'il faut opter pour la
création d'une luminance de synthèse à partir d'un mélange de H-Alpha et de
Vert...

Les sections suivantes vont présenter les trois outils disponibles dans Siril
pour faire ces mélanges de couleur et créer une image couleur.

## 1. Utiliser l'outil de Composition RVB

L'outil de [Composition
RVB](https://siril.readthedocs.io/fr/latest/processing/rgbcomp.html), est
accessible parmi les dernières entrées du menu `Traitement d'image`. Chaque
ligne de la partie supérieure, où `non chargée` est écrit, représente une image
d'entrée. Chacune d'elles se verra attribuer une couleur qui la fera contribuer
à l'image couleur finale avec une teinte.

Chargez l'une des images en cliquant sur le bouton de navigation, à gauche de
l'étiquette `non chargée`. Il peut s'agir de n'importe laquelle des images, car
nous pourrons leur réaffecter une couleur cible plus tard. Notez le cas
particulier de la première ligne qui est utilisée dans les cas où il y a une
image de luminance. Ne chargez pas d'image ici si vous ne prévoyez pas
d'utiliser la luminance dans la composition.

Faites de même pour les autres images. Pour faire apparaître une nouvelle ligne,
si besoin, cliquez sur le gros bouton `+` à gauche. Dans notre cas, nous n'avons
que trois images et nous les avons chargées dans l'ordre de la palette Hubble
(S, H, O assignés à R, V, B) :
{{<figure src="RGB_composition2.fr.png" caption="L'outil de Composition RVB avec 3 images chargées" >}}

**Remarque sur le binning et les dimensions des images**: la première image
chargée détermine la taille de l'image de sortie. Si vous avez des images de
tailles différentes, vous devez toujours charger la plus grande en premier. Si
vos images sont différentes simplement à cause du bining, donc avec le même
champ de vision, l'outil de composition met à l'échelle les images plus petites
lorsqu'elles sont chargées pour correspondre à la taille de la première image
chargée. C'est généralement utile pour le L-RVB pris avec les filtres de couleur
en bin 2. Si deux images n'ont pas été prises avec le même capteur, une
registration préliminaire devrait être faite, comme expliqué au début de ce
tutoriel.

### Alignement par translation
Il est possible d'aligner les images à partir de cet outil, en sélectionnant une
étoile et en cliquant sur le bouton `Aligner` après avoir choisi le mode
d'alignement sur une étoile. Cela ne corrigera pas la rotation entre les images
comme expliqué auparavant, dans ce cas il faudra utiliser une registration
globle, voir le début de ce tutoriel.

### Choisir la couleur pour chaque image
Maintenant, la tâche difficile de choisir une couleur qui correspond à chaque
image commence. Si vous avez chargé vos images dans un ordre aléatoire ou si
vous n'aimez tout simplement pas le résultat, vous pouvez modifier la couleur
attribuée à chaque image d'entrée. Cliquez sur la case colorée à gauche de
chaque ligne, cela ouvrira un sélecteur de couleur.
{{<figure src="color_selector.fr.png" caption="Le selecteur de couleurs apparaît après avoir cliqué sur une couleur de l'outil composition RVB" >}}

Cette fenêtre contient 12 couleurs prédéfinies: rouge, vert, bleu, cyan (un
mélange de bleu et de vert), magenta (un mélange de rouge et de bleu) et jaune
(un mélange de rouge et de vert). Alors oui, cela fait 6. Mais ces dernières
sont également dupliqués dans une version 50% plus sombre.

Lors de la sélection de la version lumineuse de ces couleurs, par exemple le
rouge vif, l'image associée sera copiée en tant que canal rouge de l'image
résultante. Si plusieurs images d'entrée doivent contribuer au canal rouge, il
ne sera alors pas une bonne idée de les rendre toutes rouges à 100%, sinon le
canal rouge du résultat sera trop lumineux et sera totalement saturé dans les
zones les plus lumineuses. Par exemple, la composition d'une image R + G + B +
H-alpha avec le H-alpha attribué au rouge nécessitera un mélange entre l'image
de filtre rouge et l'image de filtre H-alpha pour le canal rouge de l'image de
sortie. Cela peut être fait simplement à 50%-50% en sélectionnant un rouge deux
fois moins lumineux dans cette fenêtre pour ces deux images.

Pour illustrer cela, j'ai choisi ici le cyan pur, le magenta pur et le jaune pur
pour les trois images, donc chacun des canaux de couleur de l'image de sortie
(rouge, vert et bleu) a deux images en entrée qui contribuent à pleine
puissance. Le résultat est alors trop lumineux, les étoiles sont saturées.
{{<figure src="RGB_composition_CMJ1.fr.png" caption="Une coloration alternative mais trop lumineux" >}}

La bonne façon de faire aurait été ici de sélectionner les demi-couleurs de
l'outil, si vraiment il s'agissait des teintes désirées pour chaque image. En
fait, après avoir sélectionné les couleurs de pleine luminosité comme ci-dessus,
cliquer sur le bouton `Ajuster la luminosité des calques` le fera
automatiquement, mais encore mieux : si les images d'entrée n'utilisent pas la
dynamique complète, ce qui est peu probable, l'outil le détectera et autorisera
un peu plus de 50% de la luminosité à chacune. De cette façon, nous obtenons une
image non saturée :
{{<figure src="RGB_composition_CMJ2.fr.png" caption="Une coloration alternative non saturée" >}}

## Sélection de couleurs personnalisées
On pourrait être tenté d'ajuster le rapport de puissance d'un canal en fonction
des images d'entrée. Cela se fait actuellement en choisissant une couleur qui
contient cette modulation de puissance. Par exemple, pour que l'image O-III
contribue à 30% au vert et à 100% au bleu, la couleur associée à l'image devra
être exactement cela.

Cliquez sur le bouton personnalisé `+` de la fenêtre de sélection de couleur que
vous avez déjà vue. Cela changera la fenêtre en une palette de couleurs.
Malheureusement, le sélecteur de couleur que Siril utilise actuellement est
fourni de manière générique par la boîte à outils graphique qu'il utilise, et la
convention est d'utiliser l'hexadécimal pour représenter les couleurs, sur 8
bits, donc sur 256 valeurs. Donc ici, pour obtenir la couleur verte à 30%, nous
devons calculer 30% de 255 et le convertir en hexadécimal, cela devient `64`. De
nombreux outils en ligne comme
[celui-ci](https://www.joshuamiron.com/percent-to-hex-converter) peuvent vous
aider. Dans `#0064ff`, nous avons deux zéros pour le rouge, 6 et 4 pour le vert
et les deux f pour le bleu, `ff` signifie 255, ou 100%.

{{<figure src="color_selector2.fr.png" caption="L'outil personnalisé du sélecteur de couleurs utilise une représentation hexadécimale des couleurs" >}}

**Remarque** : il est équivalent de définir une couleur rouge et bleue, donc
magenta, sur une image ou d'ajouter l'image deux fois, une fois avec une couleur
rouge et l'autre avec une couleur bleue.

Après avoir choisi les teintes que vous aimez et la luminosité correspondante à
chaque canal, probablement après plusieurs itérations, vous pourrez continuer le
traitement de votre image finale. Notez que la balance des blancs n'a pas à
être parfaite à ce stade. Une neutralisation de l'arrière-plan et des opérations
de balance des blancs seront effectuées par la suite, ou d'autres outils plus
complexes.

{{<figure src="RGB_composition_custom.fr.png" caption="Une composition basée sur des couleurs personnalisées, pas forcément la meilleure solution pour cet ensemble d'images" >}}

## 2. Utiliser Pixel Math

Déjà présenté plus haut, l'outil Pixel Math est capable de mélanger plusieurs
images avec des formules. Des formules complexes existent, surtout faites pour
PixInsight qui avait cet outil à la base. Des opérateurs sont différent ou
indisponibles dans Siril, mais une bonne partie des formules devraient quand
même fonctionner.

La [documentation de l'outil](https://siril.readthedocs.io/fr/latest/processing/pixelmath.html)
est d'une grande aide. Elle fournit la liste des fonctions et opérateurs, et des
explications sur chaque élément de l'interface graphique. L'outil peut
fonctionner avec des images couleur ou monochrome, peut utiliser les formules
tapées dans les champs correspondants et ajustées pour obtenir les résultats
désirés, elles peuvent aussi être basées sur des paramètres externes qui
pourront être ajustés plus facilement dans plusieurs formules ou à plusieurs
endroits de formules complexes, et les formules peuvent être réutilisées d'une
session à l'autre avec les préréglages.

Le [tutoriel](https://siril.org/fr/tutorials/pixelmath/) montre un exemple
basique de mélange de canaux, nous verrons cela aussi plus bas car les formules
peuvent être utilisées directement dans un script. La version graphique de Pixel
Math est un très bon outil pour réaliser l'ajustement des mélanges de couleurs,
avec l'utilisation des paramètres et du bouton `Appliquer`.

## 3. Utiliser un script

Dans Siril 1.2 nous avons fait beaucoup d'efforts pour mettre à disposition les
opérations de post-traitement sous forme de commandes, et ainsi réaliser le
traitement complet d'une image de façon automatique. À partir des commandes, des
[scripts](https://siril.readthedocs.io/fr/latest/Scripts.html) peuvent être
faits, puisque ce sont simplement des listes de commandes. Toutes les opérations
dont il a été question dans ce tutorial sont disponibles:
* L'alignement des images avec la nouvelle registration globale à deux passes
  (commande `register` avec l'option `-2pass`), et les options de cadrage pour
  ne garder que la zone commune entre les images, ce qui revient à supprimer les
  zones noires (commande `seqapplyreg` avec l'option `-framing=min`).
* L'égalisation des images, avec les quatre méthodes présentées, les commandes
  `linear_match`, `pm` pour Pixel Math, `autostretch` pour l'auto-étirement, et
  `pcc` pour la calibration par photométrie.
* Le mélange de canaux peut être fait efficacement avec la version commande de
  Pixel Math, voir l'exemple plus bas.
* La nouvelle commande `rgbcomp` crée une image couleur à partir de 3 images
  monochromes ou d'une luminance et une couleur ou 3 monochromes.
* Finaliser la balance des couleurs est difficile sans voir les images, mais
  reproduire des opérations déjà faites graphiquement en réutilisant les mêmes
  paramètres est possible. Les résultat pourrait ainsi consister en une série de
  commandes pour l'égalisation des images, le mélange des canaux, les opérations
  d'étirement d'histogramme (`autostretch`, `autoghs`, `asinh`, `ght`...), de
  suppression du vert (`rmgreen`), de saturation des couleurs (`satu`), voire
  même de suppression d'étoiles (`starnet`) ou de remplacement d'étoiles
  (`synthstar`)...

Même si Pixel Math est simplifié dans sa version commande par rapport à son
interface graphique complète, il est possible de l'utiliser pour normaliser les
images et les mélanger pour obtenir un canal couleur depuis un script. Comme la
commande `rgbcomp` qui crée une image couleur ne fait que copier les données
d'entrée quand une composition sans luminance est faite, si plus de 3 images
existent, il faut bien les mélanger.

Par exemple, si les images d'entrées sont issues de filtres Rouge, Vert, Bleu et
H-Alpha, le canal rouge final pourra être obtenu en mélangeant l'image Rouge et
H-Alpha. Et Pixel Math, comme nous l'avons vu, peut faire ça très facilement.
Notez que les images sont référencées dans la version commande par leur nom
entouré du caractère `$`. Une luminance de synthèse peut aussi être créée. C'est
ce que fait cet exemple de script, intervenant après alignement et pré-étirement
des images, créant une luminance à partir de Vert et H-Alpha, et mélangeant le
Rouge et le H-Alpha dans des proportions 40/60% pour donner le canal rouge au
résultat, en partant du principe que les noms des images sont les noms de leurs
filtres:

```shell
pm "$H-Alpha$ * 0.5 + $Green$ * 0.5"
save Luminance
pm "$H-Alpha$ * 0.4 + $Red$ * 0.6"
save New_Red
rgbcomp -lum=Luminance New_Red Green Blue
```

# Étirement et finalisation de la balance des couleurs

À ce stade, les bords noirs ou monochromes de l'image couleur doivent être
supprimés s'il en subsiste. Si la calibration photométrique n'a pas été utilisée
pour en arriver là (rappelez-vous que cela ne fonctionne que pour des images
faites avec des filtres rouge, vert et bleu), la balance des couleurs devrait
quand même être proche de la teinte désirée.

Parmi les opérations habituelles à cette étape on retrouve les opérations
d'[étirement d'histogramme](https://siril.readthedocs.io/fr/latest/processing/stretching.html#generalised-hyperbolic-stretch-transformations-ghs),
les opérations d'[altération des
couleurs](https://siril.readthedocs.io/fr/latest/processing/colors.html) comme
la calibration des couleurs, la saturation des couleurs, la suppression de la
teinte verte. Le nouveau [tutoriel sur
GHS](https://siril.org/fr/tutorials/ghs/#g%C3%A9rer-la-couleur) couvre aussi
l'étirement d'histogramme pour les images couleurs, y compris SHO.

Notez que les opérations de traitement d'images qui préservent la luminosité,
comme des options de l'étirement Asinh, de GHS, de la suppression de la teinte
verte ou de la saturation des couleurs, vont aussi changer la saturation des
couleurs ou leur balance dans certains cas, et d'une façon qui ne sera pas
réversible par la suite. Elles peuvent délaver les couleurs de l'image, ou au
contraire enlever de la luminosité à des canaux de l'image si elles ne sont pas
équilibrées, ce qui arrive par exemple pour les régions H-Alpha invisibles
dans d'autres filtres.

Pour un rendu en fausses-couleurs, il est particulièrement important de faire
attention aux réglages de l'outil de suppression de teinte verte, qui va souvent
diminuer la luminosité ou la saturation des couleurs de l'image. C'est encore
plus vrai si l'outil est utilisé pour enlever une teinte magenta en mettant
l'image [en
négatif](https://siril.readthedocs.io/fr/latest/processing/colors.html#negative-transform)
avant et après l'application de l'outil.

Si la couleur et la luminosité que l'on veut obtenir sur un objet d'arrière plan
(nébuleuse, galaxie...) fait trop grossir ou saturer les étoiles, une
alternative au traitement des images est de séparer les étoiles du reste de
l'image, traiter le fond de ciel comme on l'aime puis réintégrer les étoiles de
façon maitrisée. C'est aussi possible dans Siril désormais, en utilisant
l'intéropérabilité avec
[Starnet](https://siril.readthedocs.io/fr/latest/processing/stars/starnet.html)
et l'outil de [Recomposition
d'étoiles](https://siril.readthedocs.io/fr/latest/processing/stars/star-recomp.html).
Les deux sont mis en avant dans le nouveau [tutoriel d'intégration de
Starnet](https://siril.org/fr/tutorials/integrated-starnet/).

# Résumé

Il y a plusieurs opérations à effectuer, leur ordre et leur nécessité dépendent
du type de composition de couleurs à faire. Toutes devraient démarrer avec les
images alignées et cadrées, déconvoluées et avec le gradient de fond de ciel
supprimé:
* pour du RVB, pas besoin d'égaliser les niveaux ou de faire une balance des
  couleurs, parce que PCC s'en chargera. Une simple combinaison des trois
  couleurs en une image couleur, une PCC et un étirement d'histogramme donneront
  un résultat utilisable.
* pour du L-RVB, la même chose peut être faite d'abord pour le RVB, puis
  n'oubliez pas de faire un étirement d'histogramme préliminaire avant de
  la composer avec la luminance. L'outil de Composition RVB ne prendra pas
  l'image couleur en entrée, donc pour éviter d'extraire ses canaux, la commande
  `rgbcomp` est recommandée et assez simple à utiliser.
* pour du SHO, HOO ou similaire, il vaut mieux égaliser les images d'abord,
  parce que c'est plus dur à gérer plus tard, en fonction du type d'étirement
  utilisé. Donc: normalisation, puis création d'une image couleur avec les
  mélanges ou affectations de couleurs à volonté, et enfin gérer les étirements
  et la balance des couleurs.
* pour du L-SHO et similaires, comme pour le L-RVB, le plus simple est
  probablement de créer la couleur, de pré-étirer les images et les combiner
  avec la luminance.
* pour du L-RGBHa, LRGBSHO et d'autres mélanges complexes, des mélanges de
  canaux seront nécessaires, probablement aussi pour la luminance, par exemple
  en y mélangeant le L, H et O... Une méthode qui semble bien fonctionner pour
  ajouter une seule image faite en bande fine dans une image RVB est la
  soustraction de continuum, expliquée en anglais
  [ici](https://www.nightphotons.com/guides/advanced-narrowband-combination).
