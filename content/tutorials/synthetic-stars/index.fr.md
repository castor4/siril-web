---
title: Étoiles synthétiques
author: Adrian Knagg-Baugh
show_author: true
featured_image: PMwindow.png
rate: "2/5"
type: page
---

Ce tutoriel a pour but d'expliquer les outils d'étoiles synthétiques disponibles dans Siril.
Cette fonctionnalité permet d'améliorer les profils des étoiles dans Siril.

{{< table_of_contents >}}

# Astrométrie et synthèse des étoiles
Siril dispose de puissants outils d'astrométrie qui permettent de détecter les étoiles dans les images et de les modéliser avec divers paramètres, notamment la l'amplitude maximale, la largeur à mi-hauteur, le niveau du fond environnant. Siril utilise des profils gaussiens pour modéliser les étoiles.

Vous pouvez lancer la détection et la modélisation des étoiles de plusieurs façons.
- En utilisant la commande console `findstar`.
- En utilisant la fenêtre `PSF Dynamique` (trouvée dans le menu hamburger -> `Informations de l'image`)
- Les outils Siril peuvent le faire automatiquement en arrière-plan.
Lorsque vous utilisez l'une des deux premières méthodes, Siril va détecter les étoiles et dessiner des cercles autour de toutes celles qu'il a trouvées. Les cercles orange signifient que l'étoile n'est pas saturée ; les cercles bleus signifient qu'elle est saturée.

{{<figure src="lotsofstars.png" link="lotsofstars.png" caption="Siril est capable de trouver beaucoup d'étoiles - plus de 13 000 dans cette image de la nébuleuse du Croissant !">}}

La synthèse d'étoiles est simplement le processus qui consiste à utiliser les modèles des étoiles détectées pour générer des profils mathématiques de luminance pour les étoiles. Ces profils peuvent être utilisés de plusieurs façons pour améliorer l'apparence des images.

# Que peut faire la synthèse d'étoiles pour moi ?
Il y a deux utilisations principales de la synthèse d'étoiles.
- Corriger les étoiles saturées
- Corriger des étoiles avec de mauvaises aberrations

# Interface graphique des étoiles synthétiques
L'outil d'étoile synthétique est accessible par l'interface graphique et par les commandes Siril. L'interface graphique est présentée ci-dessous :

{{<figure src="dialog.png" link="dialog.png" caption="Interface graphique de l'outil d'étoile synthétique">}}

Les commandes correspondantes à utiliser sur la console ou dans les scripts sont :

- `findstar` - cette commande appelle la routine de recherche d'étoiles avec des paramètres par défaut : on peut la considérer comme l'équivalent en commande de l'interface graphique de la `PSF Dynamique`. Elle a des paramètres optionnels `-layer=n` pour sélectionner le canal de couleur à rechercher les étoiles et `-maxstars=n` pour limiter le nombre maximum d'étoiles trouvées. Il y a aussi un paramètre `-out=/chemin/des/log.txt`, qui n'est pas nécessaire pour préparer la synthèse des étoiles.
- `setfindstar` - cette commande définit les paramètres que findstar utilise pour détecter les étoiles. Elle est destinée à un usage avancé et n'est donc pas couverte en détail dans ce tutoriel. De plus amples détails peuvent être trouvés dans l'aide de Siril.
- `unclipstars` - cette commande désature toutes les étoiles saturées dans l'image.
- `synthstar` - cette commande génère un masque d'étoiles synthétiques, synthétisant toutes les étoiles de la liste d'étoiles courante ou, si la liste est vide, à partir d'une liste d'étoiles générée en utilisant les paramètres par défaut de findstar.

# Étoiles saturées
L'image parfaite capture toute la dynamique de son sujet, des faibles volutes de nébulosité aux étoiles brillantes de première magnitude. En pratique, nos capteurs d'images ont une dynamique limitée. Ainsi, afin de capturer les détails les plus ténus, nous avons souvent des étoiles saturées. La luminosité enregistrée autour du centre de ces étoiles est juste un niveau maximum constant. Cela signifie que lorsque l'image est étirée, l'étoile gonfle et perd presque toutes ses couleurs - elle n'est plus qu'un gros point blanc brûlé. C'est l'équivalent visuel de l'horrible bruit que vous obtenez de vos haut-parleurs lorsque vous poussez un amplificateur jusqu'à l'écrêtage.

{{<figure src="satstar.png" link="satstar.png" caption="Étoiles saturées près de la nébuleuse du Pélican">}}

Les étoiles comme Sadr ont même l'air manifestement gonflées lorsque l'image n'est encore que des données linéaires non étirées.

{{<figure src="satstar-linear.png" link="satstar-linear.png" caption="Sadr, manifestement très saturé">}}

Pour améliorer cela, nous pouvons utiliser la synthèse d'étoiles. Bien que le centre du profil de l'étoile soit saturé, il y a suffisamment de détails non saturés autour du bord pour permettre de modéliser la fonction d'étalement du point (PSF) de l'étoile. Les pixels coupés dans l'étoile saturée sont remplacés par l'intensité de la PSF synthétisée générée à partir du modèle que Siril fait de l'étoile, et les valeurs de tous les pixels de l'image sont remises à l'échelle de sorte que le point le plus brillant de l'étoile la plus saturée ne soit pas plus brillant que la valeur la plus brillante que le type d'image (16 bits ou 32 bits) peut contenir. On obtient ainsi une étoile joliment profilée qui peut être étirée avec moins de gonflement.

{{<figure src="desat-linear.png" link="desat-linear.png" caption="L'étoile désaturée a un profil plus doux et moins gonflé">}}

L'effet est illustré très clairement dans une comparaison du profil d'une étoile avant et après désaturation.

{{<figure src="profiles.png" link="profiles.png" caption="L'étoile désaturée a un profil plus doux et moins gonflé">}}

Pour ce faire, c'est extrêmement facile : allez dans le menu `Traitement de l'image` et trouvez le menu `Traitement des étoiles`. Cliquez dessus, puis cliquer sur l'item `Désaturer les étoiles` et attendez quelques secondes. Félicitations, vous avez maintenant corrigé les profils de luminance de toutes vos étoiles saturées !

# Remplacement des étoiles
De temps en temps, la plupart d'entre nous prennent une image avec des étoiles épouvantables. Peut-être avons-nous oublié le correcteur de coma, peut-être avons-nous oublié de vérifier la collimation. Peut-être que nous utilisons simplement un mauvais objectif d'appareil photo. Cela arrive pour diverses raisons.

{{<figure src="badstars.png" link="badstars.png" caption="Coma dans une image de la nébuleuse Pacman">}}

Le meilleur conseil à donner dans ce cas est le suivant : refaites votre photo. Oui, il peut être exaspérant de gâcher des heures d'images, mais si vous pouvez facilement refaire l'image, vous devriez probablement le faire. Si les étoiles sont terribles, les détails du reste de l'image le seront également.

Parfois, cependant, ce n'est pas possible. Vous avez peut-être pris une image en vacances, d'une nébuleuse qui n'est pas visible depuis votre latitude, ou d'un endroit difficile à atteindre, ou encore pour une occasion spéciale. Pour une raison ou une autre, vous aimeriez bien lui épargner ces étoiles aussi affreuses.

Vous pourriez essayer la déconvolution, mais elle est sujette au bruit de l'image et il peut être difficile d'éviter les artefacts. Il n'y a pas grand-chose qu'elle puisse faire pour vous. Dans ce cas, la synthèse d'étoiles (en conjonction avec Starnet++) peut vous sauver.

L'idée est d'utiliser Starnet++ pour séparer votre image en une image sans étoiles et un masque d'étoiles. Si vous avez une installation Starnet++ fonctionnelle, vous pouvez y accéder directement depuis Siril. Il génère une image sans étoile et éventuellement un masque d'étoile. Dans ce cas, nous voulons les deux.

Gardez l'image sans étoile pour plus tard, mais pour l'instant nous ouvrons le masque d'étoile. Dans le menu `Traitement de l'image`, trouvez l'élément de sous-menu `Traitement des étoiles` et ouvrez-le.

La première chose que nous devons faire est de détecter toutes les étoiles de l'image. Vous pouvez le faire en utilisant l'outil `PSF Dynamique`, qui est accessible via le bouton de la boîte à outils à côté de l'entrée de menu `Resynthèse intégrale`, ou alternativement à partir du menu hamburger sous `Information de l'image`. Utilisez le bouton étoiles pour trouver les étoiles de votre image. Toutes les étoiles de l'image auront des cercles dessinés autour d'elles. Si vous pensez que Siril a oublié une étoile, vous pouvez dessiner une sélection autour d'elle et l'ajouter en utilisant le raccourci Ctrl-Espace. Une fois que vous êtes satisfait de la liste d'étoiles, vous pouvez fermer l'outil `PSF Dynamique` - nous n'en avons plus besoin.

Ensuite, cliquez sur l'option de menu `Resynthèse intégrale`. Cela va modéliser un profil de luminance stellaire pour chaque étoile de la liste et l'ajouter dans un masque stellaire synthétique. Si l'image originale était en couleur, la teinte et la saturation sont copiées, de sorte que les couleurs des profils d'étoiles synthétiques seront exactement les mêmes que dans l'image originale.

Si vous avez activé le mode d'affichage `Auto ajustement`, les étoiles du masque stellaire auront l'air très grosses et gonflées et manqueront de détails. Ne vous inquiétez pas, il s'agit simplement d'une bizarrerie du mode de visualisation. Elles seront bien plus belles si vous passez au mode de visualisation `Asinh`, et ceci est de toute façon plus représentatif d'un étirement typique.

Une fois que vous avez votre profil d'étoile synthétique, vous pouvez le mélanger à nouveau dans l'image sans étoile en utilisant l'outil de recomposition d'étoile de Siril. Cet outil a été écrit à l'origine simplement pour mélanger les masques d'étoiles Starnet++ avec les images sans étoiles, mais il fonctionne tout aussi bien pour mélanger les masques d'étoiles synthétiques. Les deux côtés de la boîte de dialogue vous permettent d'étirer l'image sans étoile et le masque d'étoile séparément en utilisant des étirements hyperboliques généralisés, de sorte que vous pouvez étirer les éléments faibles pour faire ressortir les détails, et appliquer un étirement beaucoup plus doux aux étoiles afin de faire ressortir les plus faibles tout en contrôlant le gonflement des plus brillantes.

{{<figure src="fixedstars.png" link="fixedstars.png" caption="Étoiles synthétiques utilisées pour améliorer l'image">}}
{{<figure src="Bubble_original.png" link="Bubble_original.png" caption="Autre exemple d'une image aux étoiles imparfaites">}}
{{<figure src="Bubble_synth.png" link="Bubble_synth.png" caption="Après intégration du masque synthétique dans Pixel Math">}}

# Conseils
- Il est très fortement recommandé d'effectuer tous les traitements d'étoiles lorsque l'image est encore linéaire, c'est-à-dire avant qu'elle n'ait été étirée. Le processus fonctionnera toujours si vous entrez une image étirée, mais les résultats peuvent être sensiblement moins bons qu'avec une image linéaire.
- La détection et la modélisation des étoiles de Siril s'attend à ce que les étoiles aient un aspect approximativement gaussien. Plus elles sont étirées, moins elles ont l'air gaussiennes, donc les performances de détection peuvent être moins bonnes et les modèles peuvent être moins bons.
- Starnet++ fonctionne également mieux avec une entrée étirée. (Ou, pour être plus précis, il fonctionne mieux avec un étirement spécifique appliqué : L'intégration starnet de Siril peut le faire pour vous si vous commencez avec une image linéaire).
- Synthetic Star Tools ne peut pas synthétiser les pics de diffraction. (Pour l'instant...)
