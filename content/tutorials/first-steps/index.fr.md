---
title: Premiers Pas -- pré-traitement avec scripts
author: Cyril Richard
show_author: true
featured_image: Capture-05.png
rate: "0/5"
type: page
---

Ce tutoriel explore vos premiers pas avec Siril et sa capacité de script.

Siril est livré avec quelques scripts de base. Ils vous permettent de prétraiter vos images en un clic!

Premier pas avec Siril et ses scripts

# 1. Créez les dossiers pour contenir vos images.

A partir de Siril 1.2.0, seuls les scripts en anglais sont fournis à l'installation, et donc les dossiers devront avec la structure suivante :
 * `biases`
 * `darks`
 * `flats`
 * `lights`

# 2. Placez vos images RAW dans les répertoires créés à l'étape précédente.

Attention ne mélangez pas les JPEG (ou autres formats) avec les fichiers brutes : Siril prendra tout ce qui se trouve dans le dossier et si des tailles/types diffèrent alors le traitement ne pourra aboutir.

# 3. Cliquez sur le bouton *home* (icone Maison en bleu) et accédez au dossier de votre projet.

Cela indique à Siril où se situent les images.

# 4. Validez avec le bouton *Ouvrir* de la fenêtre.

{{< figure src="Capture-02.png" caption="Barre d'entête de Siril" >}}

SIRIL va valider le répertoire de travail.

# 5. Cliquez sur le bouton *Scripts* et sélectionnez le script de votre choix.

Le script principal qui couvre les besoins de base est nommé `OSC_Preprocessing.ssf`.

Si vous ne voyez pas le bouton script, procédez comme suit:
1. Allez dans vos préférences (`ctrl` +` P`)
2. Supprimez tous les chemins de script (comme illustré dans la capture d'écran suivante).

  {{<figure src = "Capture-03.png" caption = "Boîte de dialogue Préférences. Les chemins de script peuvent être ajoutés ici." >}}

3. Validez vos modifications.
4. Fermez et redémarrez Siril.

   Vous êtes maintenant prêt à exécuter un script.

Si vous avez besoin de plus de scripts :
1. Cliquez sur le menu *hamburger* et sur ”Obtenir des scripts”.

   Les fichiers de script sont des fichiers texte avec l'extension `ssf`.

2. Téléchargez un script.

   Vous pouvez télécharger n'importe quel script et le mettre dans n'importe quel dossier. Il vous suffit d'indiquer à Siril le chemin où il doit le rechercher. Faites-le dans les Préférences, dans le même onglet que celui illustré ci-dessus.

   {{<figure src = "Capture-04.png" caption = "Menu principal de Siril." >}}

En fonction du nombre d'images RAW et de la puissance de votre ordinateur, Siril peut prendre un certain temps pour traiter vos images. À la fin du script, vous devriez avoir une fenêtre comme celle-ci:

{{<figure src = "Capture-05.png" caption = "Le script est en cours d'exécution." >}}

Vos images ont été calibrées, alignées et empilées. Toutes nos félicitations! Il est maintenant temps de traiter le résultat dans Siril.

Vous pouvez ouvrir l'image à traiter en cliquant sur le bouton *Ouvrir* (en haut à gauche). Votre image sera alors le fichier *result_xxx.fits* (xxx indique le temps total d'exposition en secondes).

   {{< figure src="Capture-06.png" caption="L'image prétraitée dans votre dossier." >}}

Il est normal qu'elle apparaisse sombre. Vous pouvez dès à présent passer en mode ``Auto ajustement`` (bandeau en bas) pour avoir un meilleur apperçu.

