---
title: Siril 1.2.1
author: Cyril Richard
date: 2024-01-26T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.2.1/
version: "1.2.1"
linux_appimage: "https://free-astro.org/download/Siril-1.2.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.1-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.1_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.1-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.1-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.1.tar.bz2"
---

Nous sommes ravis de vous informer que Siril 1.2.1 est maintenant disponible. De nombreux bugs ont été corrigés par rapport à la version 1.2.0 sortie l'année dernière, et nous espérons que cette version 1.2.1 sera le plus stable possible.

**Liste des bugs connus**

* Aucun pour l'instant, on croise les doigts.

**Liste des bugs corrigés (depuis la 1.2.0)**

* Correction de l'option de réduction du bruit Anscombe VST pour les images mono
* Correction de l'importation HEIF (#1198)
* Correction du bogue de la réduction du bruit Anscombe VST pour les images mono (#1200)
* Correction des problèmes avec la planification de la Transformée de Fourier > Estimation (#1199)
* Correction des bugs d'initialisation des données dans copyfits() et l'outil de composition RVB
* Correction de la colonne x exportée pour les courbes de lumière lorsque la date julienne n'est pas sélectionnée (#1220)
* Correction de la tolérance d'échantillonnage pour l'astrométrie qui était incorrectement lue (#1231)
* Possibilité de trier RA/DEC dans les fenêtres PSF (#1214)
* Ajout de SET-TEMP comme en-tête FITS valide à sauvegarder (#1215)
* Ajout d'une couleur configurable pour l'extraction de l'arrière-plan, l'échantillon et les annotations standard (#1230)
* Correction de l'erreur d'analyse des arguments dans makepsf (!593)
* Correction de l'exportation de la courbe de lumière et du csv depuis le graphe lorsque certaines images n'étaient pas sélectionnées dans la séquence (#1169)
* Ajout d'une fonction undo/redo lors de la construction de plaques avec astrometry.net (#1233)
* Correction d'un crash dans findstar lors de la détection d'étoiles proches de la frontière (#1237)
* Correction de l'utilisation de wcs info lors de l'utilisation de la commande light_curve (#1195)
* Permet de déplacer un fichier dans le répertoire de travail pour qu'il soit récupéré pour le livestacking (#1223)
* Correction de la façon dont nous vérifions s'il y a assez d'espace pour utiliser la photométrie rapide (#1238)

# Sommaire

{{< table_of_contents >}}

# Téléchargements
Siril 1.2.1 est distribué comme d'habitude pour les 3 plateformes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://siril.readthedocs.io/fr/stable/installation/source.html).

# Une version de stabilisation

Comme dit précédemment, cette version n'est qu'une version de stabilisation et n'apporte aucune nouveauté. Sa sortie est cependant importante et fait suite aux nombreux retours qui ont été fait suite à la sortie de la première version. Une fonctionnalité a cependant été ajoutée, il s'agit de la configuration des couleurs. En effet, parce que certaines personnes ont des problèmes a bien dicerner les couleurs, nous avons décidé de les rendre configurable. Dans cette version cependant, seules les annotations et les échantillons du retrait de gradient sont configurables.
{{<figure src="config_color.png" link="config_color.png" width="100%" caption="Fenêtre des préférences avec la configuration des couleurs.">}}

# Et le futur ? Siril 1.4.0, d'énormes changements

En attendant, nous ne nous contentons pas de traquer les bugs de la version stable. En effet, le développement TRÈS actif s'intensifie du côté de la version de développement, actuellement appelée Siril-1.3.0-alpha-dev. Les nouveautés apportées sont énormes et nous allons essayer d'en présenter les principales dans cette note de version.

## Siril Plot, un système d'affichage de graphiques dans Siril

Les plus grandes améliorations ne sont pas forcément les plus visibles. Pour le moment, Siril 1.2.1 nécessite l'installation de GNUPlot, un logiciel externe, pour afficher certains graphiques, notamment les courbes de lumière. Sans rentrer dans les détails, l'utilisation de programmes externes rend la maintenance compliquée, surtout pour un logiciel multiplate-forme comme Siril. Il a donc été décidé de développer notre propre module d'affichage de graphiques, afin de pouvoir afficher plus simplement un grand nombre de données. Nous appelons ce module, Siril Plot. Il offre la possibilité d’interagir avec le graphique. De zoomer, dézoomer ou bien d'enregistrer en différents formats.

## Profilage d'intensité
Ici, il s'agit probablement du plus vieux ticket qui a été ouvert dans Siril et résolu dernièrement. Il s'appuie directement sur le développement de Siril Plot et permet l'affichage de profils d'intensité lorsque l'on trace une droite sur l'image.
{{<figure src="profile.png" link="profile.png" width="100%" caption="Profile d'intensité d'une région de la photo.">}}

## Refontes de la composition L-RVB
Un des plus gros points faibles du module de composition L-RVB était la nécessité d'utiliser des images préalablement alignées en entrée, s'il y avait de la rotation de champs. Conscient de la difficulté que cela engendrait pour certains d'entre vous, nous avons amélioré l'outil pour le rendre plus simple. Il est dorénavant possible d'aligner les images en 1 clic.
{{<figure src="compositing.png" link="compositing.png" width="100%" caption="Outil de composition L-RGB, revu et amélioré.">}}

## Nouveau système de récupération de scripts externes
Vous aurez peut-être déjà remarqué dans la version 1.2.1, que lorsqu'on clique sur ``Obtenir plus de Scripts``, Siril renvoi l'utilisateur vers une page de la [documentation](https://siril.readthedocs.io/fr/stable/Scripts.html#getting-more-scripts). Cette page contient un lien vers une autre page, cette fois-ci un dépôt GitLab, qui contient de nombreux scripts tiers. N'importe qui, sous réserve de respecter la marche à suivre, peut ajouter des scripts dans ce dépôt, et il suffit ensuite à l'utilisateur de se servir. On y trouve par exemple un script de réduction d'étoiles. Dans la version de développement, nous avons simplifié le processus. Il est possible de récupérer n'importe quel script préalablement déposé sur ce dépôt à partir de Siril.
{{<figure src="gitscripts.png" link="gitscripts.png" width="100%" caption="Les scripts du dépôt sont automatiquement detectés par Siril et peuvent être installés en un clic.">}}

## Meilleure gestion des couleurs et profils ICC
La gestion des couleurs est un apport très important dans Siril 1.4. Il est maintenant possible d'utiliser des profils de couleur, personnalisés ou standard, et ainsi de produire des clichés dont le rendu des couleurs sera fidèle. Bien que pendant la plupart du processus la gestion des couleurs ne soit pas quelque chose de primordiale en astronomie, elle le devient dans l'étape finale. Encore plus si l'utilisateur jongle avec plusieurs logiciels. Ainsi, un onglet gestion des couleurs a fait son apparition dans les préférences de Siril, et le mécanisme interne de l'affichage des images a grandement évolué. Pour plus d'information, il est possible de jeter un œil à la [documentation de la version de développement](https://siril.readthedocs.io/fr/latest/Color-management.html), ou tout est expliqué en détail, avec des impressions écrans.
{{<figure src="ICC_dialog.png" caption="Fenêtre permettant de manipuler les fichiers de profile ICC de l'image chargée.">}}

## Gestion des distorsions dans l'astrométrie
À chaque version, l'astrométrie de Siril a été améliorée. Et ceci est encore vrai pour cette version de développement. Mais cette fois, l'amélioration va très loin en prenant en compte les distorsions de l'image, généralement liées à l'optique utilisée. Cette fonctionnalité est essentielle pour la gestion des mosaïques, qui est au programme des futurs développements, mais également pour les outils tels que l'étalonnage des couleurs. Il est primordial de connaître la distorsion de l'image afin de pointer précisément l'emplacement des étoiles près des bords de l'image et pas seulement proche du centre.
La prise en charge des distorsions astrométriques a pour conséquence intéressante d'être en mesure d'afficher des guides mettant en exergue les défauts optiques. Rien de tel pour connaître les erreurs de tilt ou de backfocus.
{{<figure src="BF.png" width="100%" caption="En une commande on affiche les distorsions de l'image, permettant ainsi de se représenter les défauts optiques.">}}

{{<figure src="wcs_disto.png" link="wcs_disto.png" width="100%" caption="Comparaison d'une astrométrie faite en linéaire, à gauche, et en cubique, à droite. On voit que les étoiles du catalogue (mires rouges) sont pointées très précisément (cliquer pour zoomer).">}}

## Nouvel outil d'étalonnage des couleurs par spectrophotométrie
Parce que le matériel de chacun est différent, que la réponse des capteurs CMOS n'est pas identique d'un modèle à l'autre, que la transmission des filtres est propre à chaque marque, l'étalonnage des couleurs par photométrie n'est pas précis en toute circonstance. Pour retrouver une colorimétrie précise, il est alors important de prendre en compte tous ces paramètres. C'est ce que propose l'outil d'étalonnage des couleurs par spectrophotométrie (SPCC) qui a cependant un fonctionnement très similaire. Néanmoins, contrairement à l'étalonnage des couleurs par photométrie qui peut fonctionner avec des catalogues stellaires locaux, celui-ci nécessite une connexion internet.

{{<figure src="SPCC.png" link="SPCC.png" width="100%" caption="Un exemple d'utilisation de l'outil d'étalonnage des couleurs par Spectrophotométrie en prenant des étoiles jusqu'à magnitude 17.">}}

Cet outil utilise les données de transmission des principaux filtres du marché, ainsi que des courbes de réponse des capteurs généralement utilisés en astrophotographie. Il se peut cependant que votre matériel utilisé ne figure pas dans la liste. C'est ainsi que nous avons créé un dépôt participatif, ou tout un chacun peut ajouter des données correspondant à son matériel et ainsi en faire profiter toute la communauté. Le dépôt se trouve [ici](https://gitlab.com/free-astro/siril-spcc-database) et la marche à suivre, assez simple, est expliqué dans le README. Cette base de données comporte également une liste de référence de blanc qui est utilisé dans SPCC. Nous comptons sur vous pour améliorer cet outil en aidant à créer une des plus grosses bases de données libres et open-source de matériel astrophotographique.

## Ouverture des images XISF de PixInsight, et nouveau format d'images
Une des demandes les plus communes que nous avions, concernait la possibilité d'ouvrir des images XISF, le "nouveau" format par défaut de PixInsight. C'est maintenant chose faite. Cependant, il n'est pas, et ne sera pas possible, d'enregistrer sous ce format dans Siril. Une explication est donnée dans la [documentation de cette version de développement](https://siril.readthedocs.io/fr/latest/file-formats/XISF.html).
Enfin, un nouveau format fait son apparition dans Siril, le JPEG XL. Ce format est donné pour être le remplaçant du format JPEG. Il offre une meilleure compression (possibilité sans perte) et aussi une profondeur de bit allant jusqu'à 32, idéal pour les images astronomiques. Nous ne savons pas si ce format remplacera un jour le format JPEG, mais il nous semblait intéressant de vous le proposer.

# Siril 1.4.0 c'est pour quand ?
À tous les impatients, qui après avoir lu cette note de version (soit probablement 2-3 personnes), se demandent quand sera disponible Siril 1.4.0 ? La réponse est malheureusement : pas encore. Nous avons pris l'habitude de sortir les versions quand celles-ci sont prêtes, finies et abouties. Il en sera de même pour celle-ci. De plus, il reste encore beaucoup de choses qui n'ont pas encore été commencées. Soyez patients, c'est pour votre bien.

Enfin sachez que si vous ne pouvez résister, Siril est libre et opensource. Il est facile de récupérer les sources et de compiler le projet. Il existe aussi des binaires compilés automatiquement. Mais sachez que nous déconseillons d'utiliser de telles versions (qui changent tous les jours) dans un but autre que de celui de faire des tests. Des bugs, des crashs, ou des corruptions de données pourraient se produire.

# Contribuer à Siril
Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de la documentation](https://siril.readthedocs.io/fr/stable/Issues.html) a été écrite dans le but de vous aider a reporter un problème.


# Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
