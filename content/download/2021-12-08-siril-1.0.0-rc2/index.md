---
title: Siril 1.0.0-rc2
author: Cyril Richard
date: 2021-12-08T00:00:00+01:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.0-rc2/
version: "1.0.0-rc2"
linux_appimage: "https://free-astro.org/download/Siril-1.0.0-rc2-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.0-rc2-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.0-rc2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.0-rc2.tar.bz2"
---

This version is a stabilization of the rc1 version, the new features are presented in detail [here](../2021-11-20-siril-1.0.0-rc1). We invite you to read this page carefully in order to know the implemented functions. However, we have made a short video explaining a bit how to use the improved plotting feature:

<video width="100%" controls>
  <source src="https://free-astro.org/videos/Plot_video.webm" type="video/webm">
Your browser does not support the video tag.
</video>

### Downloads
Siril 1.0.0-rc2 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). See [download](../../../download).

But of course, since Siril is free software, it can be built on any OS from source, see the [installation page](https://free-astro.org/index.php?title=Siril:install).

### Contributors
Contributors to this release are: Cyril Richard, Cécile Melis, Vincent Hourdin, Matthias Glaub.

### Translators
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
