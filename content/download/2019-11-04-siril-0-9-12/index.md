---
title: Siril 0.9.12
author: Cyril Richard
date: 2019-11-04T10:25:19+00:00
featured_image: /wp-content/uploads/2019/01/logo_light.png
categories:
  - News
tags:
  - new release
version: "0.9.12"
linux_appimage: "https://free-astro.org/download/Siril-0.9.12.glibc2.15-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_9_12_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/SiriL-0.9.12-1.app.zip"
source_code: "https://free-astro.org/download/siril-0.9.12.tar.bz2"
---

We are glad and proud to announce the release of the new Siril version: 0.9.12. This version containing a lot of improvement is accessible through the download section.

For more information about the new features, please follow this [link][1].

 [1]: https://free-astro.org/index.php?title=Siril:0.9.12
