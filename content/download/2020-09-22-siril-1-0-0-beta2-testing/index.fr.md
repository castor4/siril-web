---
title: Siril 0.99.6 (version bêta2 de la future 1.0)
author: Argonothe
date: 2020-09-22T20:50:33+00:00
featured_image: /wp-content/uploads/2018/08/pkgimage.jpg
categories:
  - Nouvelles
tags:
  - new release
version: "0.99.6"
linux_appimage: "https://free-astro.org/download/Siril-0.99.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_99_6_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.6-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.6.tar.bz2"
---

Un mois après la sortie de la première version bêta de Siril 1.0,
qui a été largement adoptée et saluée par les utilisateurs, nous publions une deuxième version bêta, portant le numéro 0.99.6. Elle corrige des problèmes importants qui avaient été trouvés dans la version précédente, la 0.99.4, grâce à l&#8217;aimable retour de notre communauté. Elle a déjà été testée par de nombreux utilisateurs et devrait
fonctionner dans la plupart des cas. N&#8217;oubliez pas qu&#8217;il s&#8217;agit d&#8217;une version bêta, mais nous la considérons déjà meilleure que la version stable précédente, la 0.9.12.

Si vous rencontrez des problèmes ou si vous trouvez des bogues, nous vous serions très reconnaissants si
vous pouvez nous contacter ou les signaler à [notre système de suivi des bogues][1].

Les points forts de la version :

  * Correction d&#8217;un bug dans l&#8217;orientation du dématriçage
  * correction d&#8217;un bug dans le paquet macOS où Siril n&#8217;était pas multithreated
  * Fuite de mémoire fixée en pixel max/min stacking
  * Crash fixé lors de la sélection de 1 pixel
  * Meilleure intégration dans un écran basse résolution
  * Ajout d&#8217;un profil ICC intégré dans les fichiers png et TIFF
  * Par défaut, Siril vérifie maintenant la mise à jour au démarrage
  * Par défaut, Siril a maintenant besoin de la commande &#8220;requires&#8221; dans le fichier Script
  * Refactoring de l&#8217;affichage des images avec capacité de panoramique
  * Bouton ajouté + et &#8211; pour la gestion des zooms

**Contributeurs** :
Les contributeurs à cette release sont : Cyril Richard, Vincent Hourdin, Ingo Weyrich (de RawTherapee) dans un merveilleux travail d&#8217;optimisation de la vitesse, Florian Benedetti, Alex Samorukov pour le bundle macOS, Guillaume Roguez, Jehan, Sébastien Rombauts, Pascal Burlot, Fabrice Faure, Julian Hofer, Hubert Fuguière, Rafael Barberá, Schreiberste, Kristopher Setnes pour le fonctionnement de X-TRANS, Cecile Melis, Floessie, Emmanuel Brandt, Matt Nohr et François Meyer (l&#8217;auteur original de Siril !!).

**Traducteurs** :
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara

Nous remercions également Colmic pour l&#8217;énorme effort de bêta-test, Laurent Rogé pour la documentation et toute la [pixls.us][2] communauté !

## Donate

Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un don d&#8217;une&nbsp; somme de votre choix.

 [1]: https://gitlab.com/free-astro/siril/-/issues
 [2]: https://discuss.pixls.us/
