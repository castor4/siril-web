---
title: Siril 0.99.6 (1.0.0 beta2 testing)
author: Cyril Richard
date: 2020-09-22T20:50:00+00:00
featured_image: /wp-content/uploads/2018/08/pkgimage.jpg
categories:
  - News
tags:
  - new release
version: "0.99.6"
linux_appimage: "https://free-astro.org/download/Siril-0.99.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_99_6_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.6-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.6.tar.bz2"
---

One month after the release of the first beta version of Siril 1.0,
which was largely adopted and praised by users, we are releasing a second beta version, with number 0.99.6. It fixes important issues that were found in the previous release, 0.99.4, thanks to the kind feedback of our community. It has already been tested by many users and should
work in most cases. Remember this is a beta version, but we consider it already better than the previous stable version, the 0.9.12.

If you have issues with it or find bugs, we would be very grateful if
you could contact us or report them to [our bug system tracker][1].

Release highlights:

  * Fixed a bug in demosaicing orientation
  * Fixed a bug in macOS package where Siril was not multithreated
  * Fixed memory leak in pixel max/min stacking
  * Fixed crash when selecting 1 pixel
  * Better integration in low resolution screen
  * Added embed ICC profile in png and TIFF files
  * By default Siril now checks update at startup
  * By default Siril now needs &#8220;requires&#8221; command in Script file
  * Refactoring of image display with pan capacity
  * Added button + and &#8211; for zooming management

**Contributors**:
Contributors to this release are: Cyril Richard, Vincent Hourdin, Ingo Weyrich (from RawTherapee) in a wonderful speed optimization work, Florian Benedetti, Alex Samorukov for the macOS bundle, Guillaume Roguez, Jehan, Sébastien Rombauts, Pascal Burlot, Fabrice Faure, Julian Hofer, Hubert Fuguière, Rafael Barberá, Schreiberste, Kristopher Setnes for X-TRANS work, Cecile Melis, Floessie, Emmanuel Brandt, Matt Nohr and François Meyer (!! the original author of Siril).

**Translators**:
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara

We also thank Colmic for the huge beta testing effort, Laurent Rogé for the documentation and all the [pixls.us][2] community!!

## Donate

Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you&#8217;re welcome to donate a small amount of your choice.

 [1]: https://gitlab.com/free-astro/siril/-/issues
 [2]: https://discuss.pixls.us/
