---
title: Siril 1.0.4
author: Cyril Richard
date: 2022-09-02T01:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.4/
version: "1.0.4"
linux_appimage: "https://free-astro.org/download/Siril-1.0.4-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.4-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.4-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.4.tar.bz2"
---

C'est la rentrée, et celle-ci s'accompagne d'une nouvelle version de Siril: la version 1.0.4. Il s'agit d'une des dernières (si ce n'est la dernière) de la branche 1.0. Cette version est principalement une version de stabilisation, mais elle contient quand même une petite nouveauté dans l'outil introduit lors de la dernière mise à jour, la transformation hyperbolique généralisée. Pour plus d'informations veuillez lire la suite.

### Téléchargements
Siril 1.0.4 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Encore une fois, cette version 1.0.4 embarque très peu de nouveautés car c'est avant tout une version de stabilisation de la 1.0.3 sortie précédemment. Elle comporte donc essentiellement des correctifs de bugs (et de crash!!) recensés par les utilisateurs et reportés sur [cette page](https://free-astro.org/index.php?title=Siril:1.0.3/fr#Probl.C3.A8me_connus). Cependant, nous ne pouvions noux résoudre à n'introduire aucune nouveauté, aussi petites soient-elles.

En effet, nous avons bien pris note de la difficulté de certains utilisateurs à utiliser le nouvel outil de transformation d'histogramme : beaucoup de curseurs et une apparence austère. Et alors que celui-ci se présente combiné avec l'histogramme dans la future version 1.2.0 en développement, nous avons introduit en attendant, un bouton qui permet de définir facilement le point de symmétrie (autrement appelé SP), essentiel pour que la transformation révèle toute sa puissance. Puisque bien souvent une vidéo vaut mieux que de grands mots, voici comment utiliser l'outil en quelques clics.

<video width="100%" controls>
  <source src="https://free-astro.org/videos/ght_1.0.webm" type="video/webm">
Your browser does not support the video tag.
</video>

1. `D` contrôle le contraste ajouté au `Point de Symétrie`, `SP`, ajustant ainsi la quantité d'étirement appliquée au reste de l'image. Augmenter `D` augmente l'intensité des étoiles dans la composition résultante.
1. `b` contrôle le degré de concentration de l'étirement en modifiant la forme de la transformation elle-même. Augmenter `b` augmente l'intensité des étoiles avec un pic de luminosité au-dessus du `Point de Symétrie`, mais peut gonfler leur PSF ; il réduira l'intensité des étoiles en dessous du `Point de Symétrie`.
1. Définit le `Point de symétrie` autour duquel l'étirement est focalisé. Cela permet de faire ressortir le faible signal sans saturer les hautes lumières.

Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


**Contributeurs**:
Les contributeurs de cette version sont : Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh. Nous tenons également à remercier tous les bêta-testeurs et notamment Fred Denjean, pour son don à trouver les bugs.

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
