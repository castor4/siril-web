---
title: Siril 1.0.4
author: Cyril Richard
date: 2022-09-02T01:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.4/
version: "1.0.4"
linux_appimage: "https://free-astro.org/download/Siril-1.0.4-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.4-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.4-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.4.tar.bz2"
---

It's back to school time, and with it, comes a new version of Siril: version 1.0.4. This is one of the last (if not the last) of the 1.0 branch. This version is mainly a stabilization version, but it still contains a small new feature in the tool introduced in the last update, the generalized hyperbolic transformation. For more information please read on.

### Downloads
Siril 1.0.4 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### What's new
Once again, this version 1.0.4 has very few new features because it is above all a stabilization version of the 1.0.3 released previously. It contains mainly bug (and crash!!) fixes reported by users on [this page](https://free-astro.org/index.php?title=Siril:1.0.3#Known_issues). However, we couldn't decide not to introduce any new features, no matter how small they are.

Indeed, we have taken note of the difficulty that some users have in using the new histogram transformation tool: a lot of sliders and an austere appearance. And while it will be combined with the histogram in the future version 1.2.0 in development, we have introduced in the meantime a button that allows to easily define the Symmetry Point (known as SP), which is essential for the transformation to reveal its full power. Since a video is often better than words, here is how to use the tool in a few clicks.

<video width="100%" controls>
  <source src="https://free-astro.org/videos/ght_1.0.webm" type="video/webm">
Your browser does not support the video tag.
</video>

1. `D` controls the contrast added at `Symmetry Point`, `SP`, thus adjusting the amount of stretch applied to the rest of the image. Increasing `D` increases the intensity of stars in the resulting composition.
1. `b` controls how tightly the stretch is focused by changing the form of the transform itself. Increasing `b` will increase the intensity of stars with peak brightness above `Symmetry Point`, but may bloat their PSF; it will reduce the intensity of stars below `Symmetry Point`.
1. Sets the `Symmetry Point` around which the stretch is focused. This brings out the weak signal without saturating the highlights.

Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/siril). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


**Contributors**:
Contributors to this release are: Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh. We also want to thank all the beta testers and especially Fred Denjean, for his gift to find bugs.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
