---
title: Siril 1.0.1
author: Cyril Richard
date: 2022-04-06T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.1/
version: "1.0.1"
linux_appimage: "https://free-astro.org/download/Siril-1.0.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.1-2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.1-2.tar.bz2"
---

Un mois après la publication de Siril 1.0.0 (première version stable), nous avons le plaisir d'annoncer la sortie de Siril 1.0.1 qui vient stabiliser encore plus cette version. En effet, bien que le développement de la branche 1.2 soit particulièrement active, nous avons décidé d'entretenir une branche 1.0 afin de fournir aux utilisateurs un suivi de cette première mouture stable.

### Téléchargements
Siril 1.0.1 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Cette version 1.0.1 embarque très peu de nouveautés car c'est avant tout une version de stabilisation de la 1.0.0 sortie le mois dernier. Elle comporte donc essentiellement des correctifs de bugs recensés par les utilisateurs et reportés sur cette [page](https://free-astro.org/index.php?title=Siril:1.0.0/fr#Known_issues). Cependant, l'outil Pixel Math a été largement amélioré, et nous allons voir les changements dans la section suivante.

Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Pixel Math, un outil en progression
Pixel Math a été écrit rapidement, peu de temps avant la sortie de la version 1.0, pour répondre aux besoins d'un nombre croissant d'utilisateurs. Cependant, contrairement à l'ensemble des fonctionnalités de Siril, cet outil manque de maturité. C'est donc dans cet esprit qu'il a été décidé d'entreprendre une amélioration de Pixel Math dans cette branche de stablisation du logiciel 1.0.X.

{{<figure src="PixelMath_dialog.png" link="PixelMath_dialog.png" width="100%">}}

Une des grandes nouveautés est que dorénavant, il est possible d'associer une formule par canal de l'image, et de fait, construire en temps réel une image couleur en fonction de différents filtres. Aussi, de nouvelles fonctions ont été ajoutées, telles que `min`, `max`, `iif` et plusieurs opérateurs important dans l'établissement de certaines expressions. Je pense notamment aux opérateurs de comparaison tels que `<`, `<=`, `>`, `>=`, `!=` et `==` et aux opérateurs logiques `&&`, `||` et `!`.

De plus, lorsque vous chargez des images dans le Pixel Math, si ces dernières possèdent le champs `FILTER` rempli, alors ce dernier devient la valeur de la variable. Par exemple, une image possédant le mot clé FITS :
```
FILTER  = 'Ha      '           / Active filter name
```
est chargée avec `Ha` comme nom de variable. Attention cependant à ce que deux images chargées ne possèdent pas le même nom de filtre. En effet, Pixel Math n'autorise pas les images à se partager des noms de variables, et dans ce cas, la deuxième image se voit attribuer un nom de variable par défaut.

Enfin, un champs `Paramètres` a été ajouté en bas de la fenêtre. Il permet d'établir des constantes que l'on souhaite utiliser dans les expressions mathématiques. Les paramètres ainsi définis peuvent être utilisés dans les expressions. Par exemple, si vous définissez des paramètres avec l'expression `k=0.8, l=0.2`, toutes les occurrences de `k` et `l` dans la formule sont remplacés respectivement par `0.8` et `0.2`. L'expression 
```
Ha * k + OIII * l
```
est donc évaluée à 
```
Ha * 0.8 + OIII * 0.2
```

Pour plus d'informations sur comment utiliser le Pixel Math, vous pouvez consulter ce [tutorial](../../tutorials/pixelmath/) qui explique en détail les fonctionnalités.

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
