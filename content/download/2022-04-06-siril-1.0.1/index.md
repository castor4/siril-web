---
title: Siril 1.0.1
author: Cyril Richard
date: 2022-04-06T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /download/1.0.1/
version: "1.0.1"
linux_appimage: "https://free-astro.org/download/Siril-1.0.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.1-2-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.1-2.tar.bz2"
---

One month after the release of Siril 1.0.0 (first stable version), we are pleased to announce the release of Siril 1.0.1 which stabilizes this version even more. Indeed, although the development of the 1.2 branch is particularly active, we decided to maintain a 1.0 branch in order to provide users with a follow-up of this first stable version.

### Downloads
Siril 1.0.1 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### So what's new
This version 1.0.1 has very few new features because it is above all a stabilization version of the 1.0.0 released last month. So it contains mainly bug fixes reported by users and available through this [page](https://free-astro.org/index.php?title=Siril:1.0.0#Known_issues). However, the Pixel Math tool has been greatly improved, and we will see the changes in the next section.

Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/c/software/siril/34). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Pixel Math, an inproved tool
Pixel Math was written quickly, shortly before the release of version 1.0, to meet the needs of a growing number of users. However, unlike all the features of Siril, this tool lacks maturity. This is the reason why we have decided to undertake an improvement of Pixel Math in this 1.0.X stablization branch.

{{<figure src="PixelMath_dialog.png" link="PixelMath_dialog.png" width="100%">}}

One of the great change is that from now on, it is possible to associate a formula per channel of the image, and in fact, to build in real time a color image according to different filters. Also, new functions have been added, such as `min`, `max`, `iif` and several important operators in the establishment of certain expressions. I am thinking in particular of comparison operators such as `<`, `<=`, `>`, `>=`, `!=` and `==` and logical operators `&&`, `||` and `!`.

Also, when you load images into the Pixel Math, if they have the `FILTER` field filled in, then its value become the name of the variable. For example, an image with the keyword FITS:

```
FILTER  = 'Ha      '           / Active filter name
```
is loaded with `Ha` as variable name. Be careful, however, that two loaded images do not have the same filter name. Pixel Math does not allow images to share variable names, and in this case the second image is given a default variable name.

Finally, a `Parameters` field has been added at the bottom of the window. It allows you to set constants that you want to use in mathematical expressions. The parameters defined in this way can be used in the expressions. For example, if you define parameters with the expression `k=0.8, l=0.2`, all occurrences of `k` and `l` in the formula are replaced by `0.8` and `0.2` respectively. The expression 
```
Ha * k + OIII * l
```
is evaluate to 
```
Ha * 0.8 + OIII * 0.2
```

For more information on how to use the Pixel Math, you can consult this [tutorial](../../tutorials/pixelmath/) which explains the features in detail.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
