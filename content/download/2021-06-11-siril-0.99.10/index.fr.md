---
title: Siril 0.99.10.1
author: Cyril Richard
date: 2021-06-23T01:00:00+02:00
categories:
  - Nouvelles
tags:
  - new release
version: "0.99.10.1"
linux_appimage: "https://free-astro.org/download/Siril-0.99.10.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-0.99.10.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.10.1-x86_64-1.dmg"
source_code: "https://free-astro.org/download/siril-0.99.10.1.tar.bz2"
---

### <span style="color:red">23 Juin, Mise à jour</span>

Suite à de nombreux rapport de bogues nous avons décidé de mettre à jour la version 0.99.10.

* Correction de la détection d'étoiles pour les images avec une résolution < 1.0
* Correction de l'interpolation lors d'alignement global
* Correction de la gestion des dates avec glib < 2.66
* Nouvel algorithm de MAD Clipping

### La version
Ceci est la quatrième version bêta de la prochaine version 1.0. Elle est livrée avec quelque nouveautés et de nombreuses corrections de bugs par rapport à la version bêta précédente (0.99.8.1) et aussi par rapport à la version stable (0.9.12), et continue le grand refactoring commencé avec la série 0.99.

Les améliorations les plus importantes sont concentrées sur l'empilement qui a subit de nombreuses améliorations, et nous tenons à remercier tout particulièrement Cécile Melis pour son travail et sa précieuse analyse sur cette nouvelle version.

### Téléchargements
En tant que version bêta de Siril 1.0, nous ne distribuons cette version qu'avec les packages pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU/Linux). Voir la page [téléchargement](../../../download).

Mais comme d'habitude, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page installation.

## Quoi de neuf dans cette version ?

### Nouvel algorithme d'alignement sur 2 ou 3 étoiles
Il se peut que parfois, notamment avec l'utilisation couplée d'une longue focale et des temps d'acquisition très courts, comme en lucky imaging par exemple, il y ait très peu d'étoiles visibles dans le champ. Pour que l'alignement global fonctionne il faut un minimum de 10 étoiles et parfois cela est beaucoup trop. Une nouvelle méthode d'alignement a donc été implémentée et fonctionne de façon semi automatique. En effet, il suffit de sélectionner 2 ou 3 étoiles les unes après les autres pour pouvoir aligner toute la séquence. Le fait d'avoir 2 ou 3 étoiles permet de prendre en compte la rotation de champ, et de fait, une nouvelle séquence est créée.

{{<figure src="3stars.png" link="3stars.png" width="100%" >}}

### Nouvel algorithme de rejet de pixel
#### Test de Déviation Extrême généralisé de Student
L'algorithme de `Test de Déviation Extrême généralisé de Student` est une généralisation du test de Grubbs qui est utilisé pour détecter une ou plusieurs valeurs aberrantes dans un ensemble de données univarié qui suit une distribution approximativement normale. En d'autres termes, cet algorithme prend comme postulat que la pile de pixel suit une distribution Gaussienne. Cet algorithme montre d'excellentes performances avec un grand ensemble de données de plus de 50 images. De plus, il a le grand avantage de ne plus avoir de pixels haut et bas à régler. Bien que deux autres paramètres existent, les valeurs par défaut de ces derniers sont généralement suffisantes et aucun ajustement n'est nécessaire.

{{<figure src="GESDT.fr.png" >}}

#### k-MAD sigma clipping
Cet algorithme a été ajouté dans la mise à jour 0.99.10.1. C'est un algorithme itératif fonctionnant comme le Sigma Clipping, excepté que l’estimateur utilisé est la MAD. C'est généralement utilisé sur le traitement d'images infrarouges très bruitées.

### Ajout de la pondération des images en fonction du bruit
Jusqu'à présent, Siril empilait les images en supposant que toutes les images contribuent de manière égale à l'image finale. En fait, cela ne conduit pas, en général, à une combinaison optimale en termes d'amélioration du rapport `Signal/Bruit`. En effet, supposons que l'une des images combinées a plus de bruit que les autres. Si nous empilons simplement cette image, alors il y'a de fortes chances que cela dégrade un peu le résultat car une partie du bruit sera traitée comme s'il s'agissait de signal.

Afin de maximiser le rapport `Signal/Bruit` dans l'image finale, il est maintenant possible d'attribuer un facteur de pondération multiplicatif à chaque image d'entrée. Les poids des images doivent rendre compte aussi précisément que possible des différences de `Signal/Bruit` existantes dans l'ensemble de données d'origine afin de donner de bons résultats. Cependant, le gain étant en général assez léger, la contribution majeure résultant de la normalisation, cette option n'est pas cochée par défaut et par design, elle n'est activable que si on choisit d'empiler avec la normalisation.

{{<figure src="weighted.fr.png" >}}

### Refonte de la normalisation
Les précédentes version de Siril normalisaient les images à partir des données calculées sur le canal de référence. Par défaut, il s'agissait du vert mais cela pouvait être changé. Bien que les résultats obtenus étaient tout à fait satisfaisants, il s'est avéré que dans certains cas cela n'était pas du tout optimal. Le choix a donc été fait de calculer la normalisation sur les 3 canaux RVB pour les images couleurs. La contrepartie de ce changement aurait dû être un temps de calcul 3 fois plus long. Cependant, nous avons également changé l'algorithme de calcul de la normalisation qui s'execute 3 fois plus rapidement qu'avant. En résumé, la vitesse de normalisation est sensiblement identique sur des images couleurs, mais 3 fois plus rapide sur des images monochromes.

{{<figure src="normalization.png" link="normalization.png" caption="Les niveaux des canaux de 4 sessions faites sous différentes conditions de ciel (présence de la lune) sont représentés avant et après la normalisation. À gauche, il s'agit de la version 0.99.8.1, à droite de la 0.9.10. On constate que la normalisation est bien plus robuste dans la nouvelle version." width="100%" >}}

### Suppression des bordures lors de l'empilement
La version 0.99.10 met fin a un des artefacts les plus embêtants de Siril, les bandes noires en bord d'images présentes après l'empilement. Ces bandes noires étaient souvent source de problème, notamment avant l'extraction de gradient de pollution lumineuse ou nous recommandions de recadrer l'image afin de les exclure avant chaque traitement. Aujourd'hui, même si ces bandes n'existent plus, un recadrage de l'image finale peut toujours s'avérer nécessaire, les bordures restant toujours plus bruitées, mais est facilité par l'absence de démarcation.
{{<figure src="border_comp.png" link="border_comp.png" caption="Les résultats d'empilement sont maintenant débarrassés des bandes noires qui pouvaient être présentes lorsque le suivi n'est pas parfait ou en cas de recouvrement imparfait entre plusieurs sessions. " width="100%" >}}

### Possibilité d'un traitement avec bias synthétique
Cette nouvelle version offre la possibilité de calibrer les images avec un bias synthétique afin de minimiser l'introduction de bruit. Cette fonctionnalité est intégralement expliquée dans ce [tutoriel](../../tutorials/synthetic-biases/).

### Extraction du canal vert
Afin d'améliorer les mesures photométriques faites avec des capteurs couleur - voir le tutoriel sur la [photométrie](../../tutorials/photometry/) - les nouvelles commandes `extract_Green` et `seqextract_Green` permettent d'extraire les 2 pixels verts du motif de Bayer et de les additionner, sur une pose unitaire ou sur une séquence. Cette opération permet d'augmenter le rapport signal/bruit en contrepartie d'une perte de résolution, mais qui est n'est pas promordiale pour les mesures de flux.

### Mise à jour du sampling après extraction de couche et/ou Drizzle
Jusqu'à la version 0.99.8.1, lorsque l'on faisait une extraction de couche `CFA`, `Ha` ou `OIII`, les informations de l'entête du fichier FITS étaient perdues. Avec la 0.99.10, non seulement elles sont gardées, mais certaines sont mises à jour. Par exemple, si la dimension des pixels est renseignée alors elle sera multipliée par 2 lors d'une extraction de couche CFA. Aussi, si l'utilisateur empile des images avec le Drizzle, la taille des pixels est cette fois divisée par 2. Ces opérations sont donc totalement transparentes avec les scripts `Extract_Ha` ou `Extract_HaOIII` car la valeur des pixels est multipliée par 2 puis divisée par 2 afin de retrouver la valeur initiale. Cependant, ce changement peut avoir son importance lorsque l'on fait un traitement normal avec un Drizzle. Nul besoin dorénavant de multiplier artificiellement la focale par 2 pour effectuer l'astrométrie. Si malgré tout, la résolution astrométrique échoue sur l'image Drizzle, une nouvelle option a été ajoutée afin d'aider les cas difficiles. Il s'agit de l'option "Sous-échantionner l'image". L'image sera réduite en interne, par l'algorithme, mais en aucun cas votre image finale ne sera modifiée.

{{<figure src="downsample.fr.png">}}

### Conservation des infos WCS dans l'image empilée
De plus en plus d'astrophotographes utilisent des solutions embarquées qui enregistrent les coordonnées WCS dans des images. Ces coordonnées sont essentielles pour effectuer une résolution astrométrique. Par conséquent, et pour simplifier cette dernière, les coordonnées WCS de l'image de référence sont maintenant transférées dans l'entête de l'image empilée. Elles sont donc importées, au même titre que la focale et la taille de pixels, en appuyant sur le bouton `Obtenir Métadonnées de l'Image`.

### Ajout du glisser déposer pour l'ouverture d'une image, ou d'une séquence
Il est maintenant possible de glisser dans la zone d'image de Siril, soit une image (au format pris en charge), soit un fichier `.seq`.

{{<figure src="drag_and_drop.fr.png" width="100%" >}}

## Tests de performance
Avec tous les changements profonds dans des algorithmes fondamentaux de Siril, nous avons décidé d'effectuer des tests de performance en comparant cette version et la version précedente, la 0.99.8.1. Les tests ont été effectués sur les deux types de fichiers FITS existants (FITS simple et FITS séquence), et en version couleur avec dématriçage dans un premier temps.

Un traitement classique est réalisé sur les 2 types de fichiers, en 16 bits et en 32 bits, qui comprend les étapes suivantes:
- prétraitement des brutes par un masterdark et un masterflat (preprocess)
- alignement global (register)
- normalisation puis empilement avec réjection (stack)

Les résultats sont représentés dans la figure ci-dessous :

{{<figure src="PerfColor.png" link="PerfColor.png" width="100%" caption="Comparaison de temps de traitement entre les versions 0.99.8.1 et 0.99.10 pour des images couleurs. Cette dernière version est plus rapide lorsque le traitement est effectué en 32bits.">}}

En 16 bits, le temps de traitement est sensiblement le même, bien qu'un peu plus long avec cette nouvelle version. Cependant, en 32 bits l'avantage va clairement à la version 0.99.10.

Le même test est réalisé ensuite sur des images monochromes et les résultats sont présentés ci-dessous. En plus des formats FITS et FITSEQ, le format SER est également ajouté à la comparaison. En effet, cette comparaison n'était pas possible avec la 0.99.8.1 pour des images couleurs, vu que le dématriçage d'un SER à l'étape de prétraitement n'était pas permis (cela l'est dorénavant avec la 0.99.10).

{{<figure src="PerfMono.png" link="PerfMono.png" width="100%" caption="Comparaison de temps de traitement entre les versions 0.99.8.1 et 0.99.10 pour des images monochromes. Cette dernière version est plus rapide quels que soient le format et la profondeur de bits.">}}

### Traducteurs
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
