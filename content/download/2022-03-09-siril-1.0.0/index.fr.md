---
title: Siril 1.0.0
author: Cyril Richard
date: 2022-03-09T08:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.0/
version: "1.0.0"
linux_appimage: "https://free-astro.org/download/Siril-1.0.0-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.0-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.0-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.0.tar.bz2"
---

10 ans après le début du développement de la branche 0.9, qui a marqué la reprise de Siril, alors abandonné, nous sommes fiers, heureux et Ô combien excités de vous offrir la version 1.0.0 en téléchargement. Cette version est l'accomplissement d'un long travail commencé en 2020 et marque le début d'une nouvelle ère. Aujourd'hui, 3 développeurs composent l'équipe, dont deux travaillant sous GNU/Linux et un autre (une autre devrais-je dire) sous Microsoft Windows, ce qui a permis de grandement améliorer la stabilité sur cette dernière plateforme.
{{<figure src="Siril-V1-welcomescreen.png" link="Siril-V1-welcomescreen.png" width="100%">}}

### Téléchargements
Siril 1.0.0 est distribué pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Cette version 1.0.0 embarque très peu de nouveautés car c'est avant tout une version de stabilisation de la 1.0.0-rc2. Il s'agit de la dernière de la série 0.99.x et la première de la série 1.0, les fonctionnalités introduites dans les versions précédentes sont donc consultables [ici](../2021-11-20-siril-1.0.0-rc1/), [ici](../2021-06-11-siril-0.99.10/) ou encore [ici](../2021-02-10-siril-0.99.8/). Nous avons corrigé un nombre important de bugs, des gros et des petits, et espérons avoir rendue cette version la plus stable possible. Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Meilleure gestion de la mémoire
Un effort particulier a été produit dans cette version afin de gérer au mieux la mémoire. En effet, avant de lancer des calculs sur une séquence, Siril détermine la mémoire nécessaire pour ajuster le nombre de calculs faisables en parallèle. Or, jusqu'à présent ce calcul n'était pas fait pour toutes les opérations et parfois le calcul avait tendance à sous-estimer la quantité de mémoire nécessaire. Si Siril dépasse la mémoire libre, le résultat est souvent une fermeture inopinée ou un blocage du système complet, ce qui a pu être observé par le passé. Le graphique ci-dessous, fait avec [cet outil](https://gitlab.com/free-astro/cpumemgraph), montre que Siril peut se limiter à 1Go de mémoire utilisée (si on lui demande) pour les opérations de pré-traitement (courbe bleue).
{{<figure src="sys_fitseq_1G.png" link="sys_fitseq_1G.png" width="100%" caption="Traitement d'une séquence FITSEQ de 90 images provenant d'une 294MC avec drizzle et une limite de mémoire définie à 1.07G dans Siril.">}}
Il reste néanmoins fortement conseillé de ne pas lancer ou utiliser d'autres logiciels pendant un traitement d'images, pour laisser le plus possible de mémoire à Siril (ça ira plus vite), mais aussi parce que si la quantité de mémoire libre varie pendant le traitement, les calculs de mémoire de Siril seraient caduques.

#### Grande amélioration de l'utilisation sous MacOS
Dans le [rapport annuel de GIMP](https://www.gimp.org/news/2021/12/31/gimp-2021-annual-report/), Jehan, le principal contributeur du logiciel, vantait les mérites de l'entraide entre les projets libres et parlait notamment de l'interaction avec Siril.

`Helping (or getting helped by) other Free Software when we can, e.g. the very nice Siril project for astronomical image processing and other software, because unlike what some think, we are not in a market or a competition! We all work together to make a better graphics work environment.`

Cette phrase prend tout son sens pour cette release, car des modifications apportées à GTK par l'équipe de GIMP, afin de rendre ce dernier beaucoup plus fluide sous MacOS, sont directement utilisables et applicables à Siril. De fait, la version 1.0.0 fonctionne beaucoup mieux dans l'environnement à la pomme : un grand merci à Jehan et Lukaso. Nous en profitons au passage pour remercier également Rafa qui signe électroniquement l'application afin que cette dernière ne soit pas rejetée par le sytème.
{{<figure src="Siril-macOS.png" link="Siril-macOS.png" width="100%" caption="Aperçu de Siril sous l'environnement MacOS en HiDPI (haute résolution).">}}

#### Nouveau standard ASTRO-TIFF
Han Kleijn, le développeur de l'excellent logiciel [ASTAP](https://www.hnsky.org/astap.htm), nous a contacté afin de mettre en place une convention de stockage des méta-données astronomiques dans les images TIFFs. Bien que le format TIFF permette l'enregistrement de méta-données, il n'existe aucun champ spécifique à l'astronomie. De fait, il était impossible de retrouver une astrométrie dans un fichier TIFF. De cette collaboration, avec également Patrick Chevalley ([Carte du ciel](https://www.ap-i.net/skychart/fr/start), [CCDCiel](https://www.ap-i.net/ccdciel/en/start), ...), est né [ASTRO-TIFF](https://astro-tiff.sourceforge.io/). Il s'agit de fichiers TIFFs (que l'on peut donc compresser sans perte) dans lesquels on va stocker l'entête d'un fichier FITS. Cela à l'énorme avantage de proposer un format lisible par tous les logiciels photos avec les entêtes d'un format astro. On peut donc dorénavant dématricer une image au format ASTRO-TIFF en toute quiétude. ASTRO-TIFF est déjà compatible avec plusieurs logiciels tels que ASTAP, CCDCiel, N.I.N.A et bientôt d'autres. Actuellement, Siril se contente de lire les fichiers ASTRO-TIFF mais n'en crée pas. Cela sera l'objet d'une évolution future. Il est cependant possible de contourner cette limitation en enregistrant le contenu de l'entête du fichier (`Clic droit->Entête FITS`) dans le champ description lors de l'enregistrement d'un fichier TIFF.
{{<figure src="workaround-astro-tiff.png" link="workaround-astro-tiff.png" width="100%" caption="Copie de l'entête FITS dans le champ description d'un fichier TIFF.">}}
{{<figure src="astro-tiff.fr.png" link="astro-tiff.fr.png" width="100%" caption="Méta-données d'une image ASTRO-TIFF telles que vu au travers d'un simple navigateur de fichiers sous GNU/Linux.">}}

#### Meilleur export vidéo
L'export vidéo a été revu et corrigé. Ce dernier est notamment plus rapide et de meilleur qualité : idéal pour s'essayer à l'[animation de comète](../../tutorials/comet/).

### Automatisation de la construction des binaires
Plus le projet grandit et plus la complexité devient importante. Par exemple, compiler Siril peut prendre jusqu'à plus de 3h sous l'environnement MacOS, il est donc important, voire obligatoire, d'automatiser toute la chaîne de production des fichiers binaires. Un effort important a été fournit en ce sens, et notamment pour les paquets MacOS et Windows. Ce dernier est maintenant compilé nativement sous Windows et la construction de l'installateur, créé à partir du logiciel [innosetup](https://jrsoftware.org/isinfo.php), est dorénavant entièrement automatisée. Quelques changements mineurs sont donc à prévoir pendant l'installation de Siril.

### Amélioration continue du site web et des tutos
Nous vous recommandons de souvent consulter le site web, et notamment la page des [tutos](../../tutorials/) qui est régulièrement mise à jour. Récemment l'index de la page a été modifié afin d'attribuer une note de 0 à 5, exprimant la difficulté abordée dans le sujet. Cette note est là à titre indicatif, et nous invitons bien évidemment tous les utilisateurs à assouvir leur curiosité.

