---
title: Siril 1.0.0
author: Cyril Richard
date: 2022-03-09T08:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.0/
version: "1.0.0"
linux_appimage: "https://free-astro.org/download/Siril-1.0.0-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.0-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.0-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.0.tar.bz2"
---

10 years have passed since branch 0.9 restarted the then-dropped development of Siril. So we are extremely proud, happy and thrilled to release Siril 1.0.0 for download. This version is the result of a period of hard work started in 2020 and we hope it opens the beginning of a new era. Today, Siril team consists of 3 devs, the historical duo working under GNU/Linux environment, while the last team member works under Microsoft Windows, which has helped a lot stabilizing Siril behavior for this OS.
{{<figure src="Siril-V1-welcomescreen.png" link="Siril-V1-welcomescreen.png" width="100%">}}

### Downloads
Siril 1.0.0 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### So what's new
Siril 1.0.0 is a stabilisation version of the recent release candidates with very few features added. It puts an end to the 0.99.x series and inaugurates series 1.0.x. All the features introduced over the course of the previous releases are shown [here](../2021-11-20-siril-1.0.0-rc1/), [here](../2021-06-11-siril-0.99.10/) and [there](../2021-02-10-siril-0.99.8/). We have corrected a large number of bugs, some small, some big, and hope that we are delivering a very stable version. Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/c/software/siril/34). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### Better memory management
Special effort has been made in this version to improve memory management. Before starting a computation on a sequence, Siril computes the required memory to adjust the number of threads that can be used. Until now, this was not done for all operations, or in a way that under-estimated the required memory. If Siril overshoots the available memory, the results is often a software crash or system freeze, which some users have seen with previous versions. The graph below, made with [this tool](https://gitlab.com/free-astro/cpumemgraph), shows that Siril can limit itself to 1 GB of memory (if configured to), for operations of preprocessing (blue curve).
{{<figure src="sys_fitseq_1G.png" link="sys_fitseq_1G.png" width="100%" caption="Processing a single-file FITSEQ sequence of 90 images from a 294MC with drizzle and a memory limit set to 1.07GB in Siril.">}}

We still stongly recommend not launching or using other software while Siril is working, to leave as much memory as possible to Siril (which will make it process faster), but also because if the amount of free memory changes during processing, the memory computation of Siril would be incoherent and possibly lead to the mentioned errors.

#### Huge improvements for macOS users
In his [last annual report](https://www.gimp.org/news/2021/12/31/gimp-2021-annual-report/), Jehan, GIMP's main developper, was praising the spirit of mutual support that exists between projects in the free open source software ecosystem:

`Helping (or getting helped by) other Free Software when we can, e.g. the very nice Siril project for astronomical image processing and other software, because unlike what some think, we are not in a market or a competition! We all work together to make a better graphics work environment.`

This sentence means a lot in the context of this release, as some mods made to GTK by GIMP devs to make macOS interactions much more fluid, have been ported directly into Siril. Version 1.0.0 runs much more smoothly on macOS systems and we want to send a big shout-out to Jehan and Lukaso. We also take the opportunity to thank Rafa who notarizes Siril builds under this OS so that they are not rejected by the system.
{{<figure src="Siril-macOS.png" link="Siril-macOS.png" width="100%" caption="Siril in MacOS environment.">}}

#### New ASTRO-TIFF standard
Han Kleijn, dev of the awesome [ASTAP](https://www.hnsky.org/astap.htm), contacted us to set a new convention to store astronomy metadata in TIFF images. While TIFF format already allowed to store metadata, there was no specific field dedicated to astronomy data. For instance, it was not possibile to store and retrieve results from an astrometry solve in a TIFF file. From this collaboration, also involving Patrick Chevalley ([Carte du ciel](https://www.ap-i.net/skychart/fr/start), [CCDCiel](https://www.ap-i.net/ccdciel/en/start), ...), [ASTRO-TIFF](https://astro-tiff.sourceforge.io/) was born.
ASTRO-TIFFs are TIFF files (that can be compressed lossless) into which a FITS header is stored in their `Description` field. This presents the huge benefit to be readable by any image editing software while preserving astro images information. You can for instance debayer an ASTRO-TIFF image without having to bother specifying its Bayer pattern as this info can be read directly from its metadata. ASTRO-TIFF is already implemented in ASTAP, CCDCiel, N.I.N.A and more to come. For the time being, Siril only reads ASTRO-TIFF files but does not write them, although this is already in the dev plan. You can still get around this limitation by pasting the FITS header data (`right clic-> FITS Header`) in the description field when saving your image as a TIFF.
{{<figure src="workaround-astro-tiff.png" link="workaround-astro-tiff.png" width="100%" caption="Pasting FITS header data upon saving a TIFF image.">}}
{{<figure src="astro-tiff.fr.png" link="astro-tiff.fr.png" width="100%" caption="ASTRO-TIFF file metadata as seen in a file browser under GNU/Linux.">}}

#### Better video export
Video export has been reviewed and corrected. It is notably faster and better: you can test it by making a nice [comet animation](../../tutorials/comet/).

### Automating builds
As the project grows, delivering builds for all the systems becomes more and more complex. For instance, building Siril under macOS environment can take more than 3 hours and deals with thousands of files. It is therefore important, if not mandatory, to automate the full chain to deliver the package bundle. A huge effort has been put into automating all the processes, in particular for MacOS and Windows. The latter is now compiled natively and generating the installer with [innosetup](https://jrsoftware.org/isinfo.php) is also fully automated.

### Improving existing and delivering new tutorials
We strongly recommand you visit frequently our website, in particular the [tutorials](../../tutorials/) page, which is regularly filled with new and/or updated content. Recently, the index page has been remodelled to rank them by level of difficulty from 0 to 5. The ranking is, of course, subjective, and we encourage all users to read tutorials whatever their grade to quench their thirst for knowledge.

