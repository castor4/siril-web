---
title: Siril 1.2.0
author: Cyril Richard
date: 2023-09-15T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /download/1.2.0/
version: "1.2.0"
linux_appimage: "https://free-astro.org/download/Siril-1.2.0-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.0-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.0_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.0-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.0-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.0.tar.bz2"
---

We are delighted to inform you that Siril 1.2.0 is now available. Many bugs have been corrected since the previous release candidate, making this new version ready to become the final version.

This version doesn't bring anything new compared to version 1.2.0-rc1, but it does include a large number of bug fixes. In fact, as the list of users grew ever larger, so did the number of bug reports we received, enabling us to fix a large amount of issues. Many thanks to you all.

**List of known bugs**

* None yet, fingers crossed.

**List of fixed bugs (from rc1)**

* Fixed crash in background extraction samples removing (#1123)
* Fixed crash in binning with ushort images (4d4d4878)
* Fixed crash in findstar when a large saturated patch was close to border
* Fixed memory leaks in deconvolution code (3e122ad7)
* Fixed sorting in the rightmost columns in Dynamic PSF window (75586c04)
* Added logging typed command in stdout (06f67292)
* Improved path-checking and messages for astrometry.net local solver (Windows) (!524)
* Prevent crash using recomposition tool eyedroppers without images loaded (!526)
* Fixed HaOiii script failure if input image has odd dimensions (!533)
* Fixed errors in GNUplot process handling (!538)
* Fixed crash with seqextract_HaOIII command (!535)
* Fixed crash when trying to export csv from empty plot (#1150)
* Fixed deleting RA/Dec info when undoing an astrometry solve (#1119)
* Improved error detection and messages for astrometry (!516)
* Fixed sampling range specification for siril internal solver (!549)
* Fixed all command descriptions (!546)
* Fixed SER orientation after AVI conversion (#1120)
* Fixed rescaling float images upon loading when not in [0,1] range (#1155)
* Fixed initialization of guide image for clamping (#1114)
* Fixed disabling weighting in the GUI when widget is not visible (#1158)
* Fixed output_norm behavior for stacking to ignore border values (#1159)
* Fixed the check for the no space left condition in the conversion (#1108)
* Fixed DATE_OBS missing on seqapplyreg if interp was set to NONE (#1156)
* Fixed photometry issue with 8-bit .ser file (#1160)
* Fixed removing zero values in histogram calculations (#1164)
* Fixed pixel sizes after HaOIII extrcation (#1175)
* Fixed crash when passing only constants in pm expression (#1176)
* Fixed incorrect channel when adding star from selection in RGB vport (#1180)
* Allow to access to non-local disk (#1182)
* Allow the Star Recomposition UI to scroll when larger than the dialog (#1172)
* Fixed wrong Bayer interpretation after FITS-SER conversion (#1193)
* Fixed pixelmath argument parsing error (#1189)

# Table of Contents

{{< table_of_contents >}}

# Downloads

Siril 1.2.0 is distributed for the 3 most common platforms (Windows, macOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://siril.readthedocs.io/en/stable/installation/source.html) page.

# What are the major changes in this version since last stable version, 1.0.6

The number of changes is very large, probably larger than between versions 0.9.12 and 1.0.0. It would take too long, and I don't think anyone would read them, to write detailed release notes of every new feature. So we decided to focus on the most important ones, and if possible, to refer to the tutorials or our brand new documentation.

The complete list of changes/improvements/corrections, aka the Changelog, can be found [here](https://gitlab.com/free-astro/siril/-/raw/1.2.0/ChangeLog).

* To see the evolutions brought by the beta versions, go [here](../2023-02-24-siril-1.2.0-beta1/) and [here](../2023-03-12-siril-1.2.0-beta2).
* To see the evolutions brought by the rc version, go [here](../2023-06-01-siril-1.2.0-rc1).

# Contribute to Siril
Of course, Siril is a computer program developed by fallible humans, and bugs may still exist. If you think you've discovered one, please [contact](.../.../faq/#how-can-I-contact-siril-team-) or [write](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) a bug report if it isn't already present in the [list of known bugs](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). A [documentation page](https://siril.readthedocs.io/en/stable/Issues.html) has been written to help you report a problem.


The **contributors** of this version are : 

- **Cyril Richard**
- **Vincent Hourdin**
- **Cécile Melis**
- **Adrian Knagg-Baugh**
- **René de Hesselle**
- Fred DJN
- Alexander
- Antoine Hoeffelman
- Garth TianBo
- Jehan
- Jens Bladal
- Joan Vinyals Ylla Català
- Mario Haustein
- Rafel Albert
- Stefan Beck
- Udo Baumgart
- Victor Boesen Gandløse
- Yi Cao
- Yoshiharu Yamashita
- Zachary Wu


# Donate

Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
