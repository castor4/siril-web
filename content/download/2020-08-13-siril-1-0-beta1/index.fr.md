---
title: Siril 0.99.4 (version bêta de la future 1.0)
author: Argonothe
date: 2020-09-07T12:11:04+00:00
featured_image: /wp-content/uploads/2020/08/siril_logo.png
categories:
  - Nouvelles
version: "0.99.4"
linux_appimage: ""
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_99_4_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.4-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.4.tar.bz2"
---

Nous pensons qu&#8217;il est temps de sortir cette nouvelle version pour un test bêta.

Depuis la dernière version stable, la 0.9.12, plus de 1400 commits ont été poussés vers notre dépôt, plus de 66 000 nouvelles lignes de code ont été ajoutées. De nombreux nouveaux développeurs nous ont aidés à optimiser, à ajouter de nouvelles fonctionnalités et à corriger des bogues. C&#8217;est sans aucun doute notre plus grande amélioration du code de Siril.

C&#8217;est pourquoi, contrairement à ce que nous faisions auparavant, nous avons décidé de publier une version bêta pour les trois plateformes les plus courantes. Cette version, la 0.99.4, a été testée pendant un certain temps par quelques utilisateurs chanceux et devrait fonctionner dans la plupart des cas. Si vous avez des problèmes avec elle ou si vous trouvez des bogues, nous vous serions très reconnaissants de nous contacter ou de les signaler à notre système de suivi des bogues.

Les points forts de la version :

Refonte complète de l&#8217;interface utilisateur
Précision de 32 bits
Nouveaux paquets MacOS et Windows
Nouvelles écritures universelles (DSLR/FITS)
Plusieurs corrections de bogues

Contributeurs :
Les contributeurs à ce communiqué sont : Cyril Richard, Vincent Hourdin, Ingo Weyrich (de RawTherapee) dans un merveilleux travail d&#8217;optimisation de la vitesse, Florian Benedetti, Alex Samorukov pour le bundle macOS, Guillaume Roguez, Jehan, Sébastien Rombauts, Pascal Burlot, Fabrice Faure, Julian Hofer, Hubert Fuguière, Rafael Barberá, Schreiberste, Kristopher Setnes pour le travail sur X-TRANS, Cecile Melis, Floessie, Emmanuel Brandt et François Meyer ( ! ! l&#8217;auteur original de Siril).

Traducteurs :
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara

Nous remercions également Colmic pour l&#8217;énorme effort de bêta-test, Laurent Rogé pour la documentation et toute la communauté pixls.us !

## Donate

Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un don d&#8217;une&nbsp; somme de votre choix.

<div style='margin-left: auto;margin-right: auto;width:220px'>
</div>
