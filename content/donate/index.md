---
title: Donate
author: Cyril Richard
type: page
date: 2020-09-06T11:09:49+00:00
menu: "main"
weight: 10
---

Developing software is fun, but it also takes up nearly all of our spare time. 
If you like Siril and would like to support us in continuing development you’re welcome to donate a small amount of your choice.


### PayPal

We are using PayPal to [accept donations][]:

<img class='mb-2' src="/images/PP QR Code.png" alt="PayPal QR Code" width="142"/>

<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="business" value="paypal@free-astro.org" />
<input type="hidden" name="currency_code" value="EUR" />
<input type="image" src="/images/btn_donate_pp_142x27.webp" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
</form>

### Liberapay
[Donate to Siril on Liberapay](https://liberapay.com/Siril). Liberapay is a platform run by a non-profit organization allowing monthly crowdfunding (subscription based). See Wikipedia’s page on [Liberapay](https://en.wikipedia.org/wiki/Liberapay).

### Dogecoin

Dogecoin (DOGE) is a cryptocurrency created by software engineers Billy Markus and Jackson Palmer, who decided to create a payment system that is instant, fun, and free from traditional banking fees. For more details see Wikipedia’s [Dogecoin](https://en.wikipedia.org/wiki/Dogecoin) article.

Dogecoin address: `D9LBbHKphb9aiJ22r8yeKijT4igj1Bd3Ri`
{{<figure src="/images/icons/dogecoin.svg" width="125">}}


[accept donations]: https://www.paypal.com/donate?business=paypal%40free-astro.org&item_name=Siril+and+the+free-astro+team&currency_code=EUR
