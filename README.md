# Siril Website

Beginning a theme and static-site migration for Siril.


## Cloning

The theme for the site uses https://gitlab.com/patdavid/hugo-bootstrap-bare as a git submodule.
In order to clone this submodule along with the site you just need to add the flag `--recurse-submodules` to the clone command:

    git clone --recurse-submodules git@gitlab.com:free-astro/siril-web.git

If you already have the site cloned, but haven't included the submodule before:

    git submodule update --init --recursive


## Requires

* Hugo  
    Currently `Hugo Static Site Generator v0.79.0/extended`  
    Grab the latest **extended** version of Hugo: https://gohugo.io/getting-started/quick-start/#step-1-install-hugo
* Node  
    Grab a recent Node/npm (or yarn if you prefer): https://nodejs.org/en/

## Hugo

This site is built with the static site generator Hugo (extended).
Currently:
```
./hugo.exe version
Hugo Static Site Generator v0.79.0/extended windows/amd64 BuildDate: unknown
```

### SASS

**If cloning the repo fresh**, remember to build the bootstrap assets locally:

```
cd themes/hugo-bootstrap-bare/assets/
npm install (or alternatively `yarn install`).
```


## Updating

If you have the repo cloned but need to update things, it helps to make sure everything is up to date (since we are also using a submodule for the base theme).

As normal, from the project root directory, update things:
```
git pull
```

Double check that the submodule is being updated as well:
```
git submodule update --init --recursive
```

And finally make sure the assets are built:
```
cd themes/hugo-bootstrap-bare/assets/
yarn install (or alternatively `npm install`).
```
This should get things up and running. If not - yell at @patdavid or @paperdigits.


## Build and Test

Building the site to test locally can be done from the root of the repo:

```
hugo server -D --disableFastRender
```

The site should then be available at http://localhost:1313/

## Build

To simply build the site you can run:

```
hugo
```

The website will be built into the `/public/` directory.


# Publishing
The main branch will publish to https://siril.org.
The `staging` branch will publish to https://staging.siril.org

Don't forget to keep them in sync if you don't want to be chasing down commits and rebasing.. :)
